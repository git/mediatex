#!/bin/bash
set -x
set -e

# By chance we can backport up to wheezy without applying any patch.

# note: it may helps
# # apt-get install ubuntu-archive-keyring

# Z in mediatex_X.Y-Z_i386.deb
DEBIAN_REVISION=2

V=$DEBIAN_REVISION
MIRROR_DEBIAN='http://ftp2.fr.debian.org/debian'
MIRROR_UBUNTU='http://ftp.ubuntu.com/ubuntu'

#         0           1             2           3	      4         
CODENAME=("stretch"   "trusty"      "jessie"    "precise"     "wheezy" )
REVISION=("$V"        "$V~bpo14.04" "$V~bpo8"   "$V~bpo12.04" "$V~bpo7")
MIRROR=(  "debian"    "ubuntu"      "debian"    "ubuntu"      "debian")

# get sources
function getSources {
    mkdir -p dist
    cd dist
    rm -fr mediatex*
    git clone git://git.savannah.nongnu.org/mediatex.git
    cd mediatex
    autoreconf -i | tee ../../log/src.log
    ./configure | tee -a ../../log/src.log
    cd ../..
}

# build distribution
# $1: version
function buildDist {
    cd dist/mediatex
    make | tee ../../log/dist.log
    make dist | tee -a ../../log/dist.log
    cd ..
    mv mediatex/mediatex-${1}.tar.gz mediatex_${1}.orig.tar.gz
    cd ..
}

# get pbuilder environement
# $1: distrib's codename
# $2: distrib's mirror
function buildEnv {
    mkdir -p env
    cd env
    rm -f $1.tgz

    if [ "$2" == "debian" ]; then	
	pbuilder create --distribution $1 \
		 --basetgz $1.tgz \
		 --mirror $MIRROR_DEBIAN \
	| tee ../log/env-${1}.log

    else
	# need universe for libavl
	pbuilder create --distribution $1 \
		 --basetgz $1.tgz \
		 --mirror $MIRROR_UBUNTU \
		 --othermirror "deb $MIRROR_UBUNTU $1 universe" \
	    | tee ../log/env-${1}.log
    fi
    
    cd -
}

# get debian directory
# $1: distrib's codename
# $2: version
# $3: revision
function getDebian {
    mkdir -p deb
    cp dist/mediatex_${2}.orig.tar.gz deb
    cd deb
    rm -fr mediatex-${2}
    tar -zxf mediatex_${2}.orig.tar.gz
    cp -fr ../dist/mediatex/debian mediatex-${2}

    sed mediatex-${2}/debian/changelog -i \
	-e "1 imediatex (${2}-${3}) ${1}; urgency=medium" \
	-e '1 d'
    
    cd ..
}

# build packages
# $1: distrib's codename
# $2: version
function buildPackage {
    cd deb/mediatex-${2}

    pdebuild --use-pdebuild-internal \
    	     --debbuildopts "-sa $BUILDOPT" -- \
    	     --basetgz ../../env/$1.tgz \
    	| tee ../../log/deb-${1}.log
    cd ../..

    mkdir -p repo
    cp dist/mediatex_${2}.orig.tar.gz repo
    mv deb/*_${2}-* repo
}

# main
mkdir -p log
getSources    
VERSION=$(grep AC_INIT dist/mediatex/configure.ac | cut -d '[' -f3 | cut -d ']' -f1)
buildDist $VERSION

for I in 0 1 2 3 4; do
    echo ${CODENAME[$I]} ${REVISION[$I]} \"${MIRROR[$I]}\"
    buildEnv ${CODENAME[$I]} "${MIRROR[$I]}"
done

for I in 0 1 2 3 4; do
    echo ${CODENAME[$I]} ${REVISION[$I]}
    
    if [ \( $I -eq 0 \) -a \( $(dpkg --print-architecture) == "i386" \) ]; then
	# source paquet generated
	BUILDOPT=""
    else
	# source paquet not generated
	BUILDOPT="-b"
    fi

    getDebian ${CODENAME[$I]} $VERSION ${REVISION[$I]}
    buildPackage ${CODENAME[$I]} $VERSION
done

echo "Done"

# bellow should be done (manualy) to store packages using reprepro
# store package
# $1: distrib's codename
# $2: version
#function storePackage {
#    cp deb/*_${2}-* /var/lib/reprepro/incoming
#    (reprepro -b /var/lib/reprepro/ remove $1 mediatex libmediatex1 libmediatex-dev)
#    reprepro -b /var/lib/reprepro/ processincoming incoming
#    reprepro -b /var/lib/reprepro/ export
#}

# bellow can be use to build a package depending on libmediatex1.
# (I still do not find solution to pass the othermirror's key.)
#
# pbuilder create --distribution stretch \
# 	 --basetgz stretch2.tgz \
# 	 --mirror http://ftp2.fr.debian.org/debian \
# 	 --othermirror "deb [trusted=yes] http://www.narval.fr.eu.org/debian stretch main"
#
# pdebuild --use-pdebuild-internal \
#     	     --debbuildopts "-sa -b" -- \
#     	     --basetgz ../../stretch.tgz
