#!/bin/bash
#=======================================================================
# * Project: MediaTex
# * Module : script libs
# *
# * This module manage the mdtx's ssh keys.
#
# MediaTex is an Electronic Records Management System
# Copyright (C) 2014 2015 2016 2017 2018 Nicolas Roche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#=======================================================================
#set -x
set -e

###
# Rq: it helps a lot !
# # /usr/sbin/sshd -D -ddd
# # ssh -vvv bibi
###

# includes
MDTX_SH_SSH=1
[ -z $srcdir ] && srcdir=.
[ -z $libdir ] && libdir=$srcdir/../scripts/lib
[ ! -z $MDTX_SH_LOG ] || source $libdir/log.sh
[ ! -z $MDTX_SH_INCLUDE ] || source $libdir/include.sh

MDTX_KEY_HAVE_CHANGE=0

# build ssh keys without password
# $1: collection user
function SSH_build_key()
{
    Debug "$FUNCNAME: $1 '$2'" 2
    [ $# -eq 2 ] || Error "expect 2 parameters"
    [ ! -z "$1" ] || error "need a collection label"
    
    # /var/cache/mediatex/mdtx/home/$1/.ssh
    COLL_SSHDIR=$HOMES/$1$CONF_SSHDIR
    SSH_KNOWNHOSTS=$COLL_SSHDIR$CONF_SSHKNOWN
    COMMENT="${1}@$(hostname -f)"     # Warning: this may fails
    KEY=$2

    # use provided key if available
    if [ ! -z "$KEY" ]; then
	Notice "use provided key: $KEY"
	[ -e $KEY ] || Error "cannot load $KEY private key"
	[ -e $KEY.pub ] || Error "cannot load $KEY.pub public key"
	cp $KEY $COLL_SSHDIR/id_rsa
	cp $KEY.pub $COLL_SSHDIR/id_rsa.pub
    fi

    if [ \( -e $COLL_SSHDIR/id_rsa \) -a \
	 \( -e $COLL_SSHDIR/id_rsa.pub \) ]; then
	Info "re-use the previous keys we found"
    else 
	MDTX_KEY_HAVE_CHANGE=1
	ssh-keygen -t rsa -f $COLL_SSHDIR/id_rsa -P "" -C "$COMMENT" \
		   >/dev/null 2>&1 || Error "cannot generate ssh keys"
    fi
    chmod 644 $COLL_SSHDIR/id_rsa.pub
    chmod 600 $COLL_SSHDIR/id_rsa
    chown $1:$1 $COLL_SSHDIR/*

    # reset known servers keys
    rm -f $COLL_SSHDIR/$SSH_KNOWNHOSTS
}

# bootstrap ssh configuration with local keys
# $1: collection user
function SSH_bootstrapKeys()
{
    Debug "$FUNCNAME $1" 2
    [ ! -z "$1" ] || error "need a collection label"
    
    # /var/cache/mediatex/mdtx/home/$1/.ssh
    COLL_SSHDIR=$HOMES/$1$CONF_SSHDIR
    SSH_KNOWNHOSTS=$COLL_SSHDIR$CONF_SSHKNOWN

    # add our public key to accept self login
    install -o $1 -g $1 -m 644 $COLL_SSHDIR/id_rsa.pub \
	    $COLL_SSHDIR/authorized_keys

    # add localhost public key to accept blind connection
    echo -n "127.0.0.1 " > $SSH_KNOWNHOSTS
    cat /etc/ssh/ssh_host_rsa_key.pub >> $SSH_KNOWNHOSTS
    ssh-keygen -H -f $SSH_KNOWNHOSTS 2>/dev/null
    rm -f $SSH_KNOWNHOSTS.old

    chmod 644 $SSH_KNOWNHOSTS
    chown $1:$1 $SSH_KNOWNHOSTS
}

# configure ssh for the first connection
# (config file is next managed by ssh.c)
# $1: user
# $2: host
# $3: port
# $4: strictHostKeyChecking ('ask' or 'no' ; default is ask)
function SSH_configure_client()
{
    Debug "$FUNCNAME $1 $2 $3 $4" 2
    [ $# -eq 4 ] || Error "expect 4 parameters"
    
    # /var/cache/mediatex/mdtx/home/$1/.ssh/config
    SSH_CONFIG=$HOMES/$1$CONF_SSHDIR$CONF_SSHCONF

    cat > $SSH_CONFIG <<EOF
Host *
  # question about unknown remote host
  strictHostKeyChecking $4
   
  # use rsa first\n"
  HostKeyAlgorithms ssh-rsa,ssh-dss

# default ports
Host $2
  Port $3
EOF
    chown $1:$1 $SSH_CONFIG
}

# enable ssh login into chroot
# $1: yes or no
function SSH_chroot_login()
{
    Debug "$FUNCNAME: $1" 2
    [ ! -z "$1" ] || error "need a 'yes|no' parameter"
    [ $(id -u) -eq 0 ] || Error $0 $LINENO "need to be root"

    # protect stars from shell expension
    PATTERN="$MDTX-*"
    TMP_STRING="/# <<<$PATTERN/,/# $PATTERN>>>/ d"
    SED_STRING=$(echo $TMP_STRING | sed -e 's/*/\\\*/g')
    sed -i -e "$SED_STRING" /etc/ssh/sshd_config
    if [ "$1" = "yes" ]; then
	cat >> /etc/ssh/sshd_config <<EOF
# <<<$PATTERN
Match user $PATTERN
          ChrootDirectory $JAIL
          X11Forwarding no
          AllowTcpForwarding no
# $PATTERN>>>
EOF
    fi
    invoke-rc.d ssh reload
}

