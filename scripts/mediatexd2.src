#!/bin/sh
if [ true != "$INIT_D_SCRIPT_SOURCED" ] ; then
    set "$0" "$@"; INIT_D_SCRIPT_SOURCED=true . /lib/init/init-d-script
fi
### BEGIN INIT INFO
# Provides:          mediatexd
# Required-Start:    $remote_fs $syslog
# Required-Stop:     $remote_fs $syslog
# Default-Start:     2 3 4 5
# Default-Stop:      0 1 6
# Short-Description: Mediatex daemon
# Description:       Mediatex server startup script
### END INIT INFO

# Author: Nicolas Roche <nroche@narval.fr.eu.org>
#set -x
#ulimit -c unlimited # produce core-dump

USER=mdtx
NAME=mediatexd
DESC="Mediatex archival daemon ($USER)"
PIDDIR=CONF_LOCALSTATEDIR/runCONF_MEDIATEXDIR
PIDFILE=$PIDDIR/${USER}d.pid
DAEMON=/usr/bin/mediatexd
START_ARGS="--user $USER"
DAEMON_ARGS="--conf-label $USER --background
 --facility local2 --severity notice"

#
# /var/run/mediatex is lost when system halt, because now /var/run
# is a tmpfs filesystem
# 
do_start_prepare()
{
    if [ ! -d $PIDDIR ]; then
	mkdir -m 0777 $PIDDIR
    fi
}

#
# Function that sends a SIGHUP to the daemon/service, 
# so as daemon can reload its configuration without restarting
#
do_reload()
{
    log_daemon_msg "Reloading $DESC configuration files" "$NAME"
    printf "\n"
    start-stop-daemon --stop --signal HUP --quiet \
		      --pidfile $PIDFILE
    return 0
}

#
# Overload to change user and signal delais
#
do_stop_cmd_override() {
    start-stop-daemon --stop --quiet --retry=TERM/10/KILL/5 \
		      --pidfile $PIDFILE --exec $DAEMON --user $USER
    RETVAL="$?"
    [ "$RETVAL" = 2 ] && return 2
    rm -f $PIDFILE
    return $RETVAL
}

#
# Overload to pass PIDFILE
#
do_status() {
    status_of_proc -p "$PIDFILE" "$DAEMON" "$NAME" && return 0 || return $?
}

# Local Variables:
# mode: shell-script 
# mode: font-lock
# mode: auto-fill
# End:
