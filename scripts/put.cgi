#!/usr/bin/perl -w
#=======================================================================
# * Project: MediaTex
# * Module : Upload cgi script
# *
# * This cgi-script make glue between put.shtml and put.sh scripts
#
# MediaTex is an Electronic Records Management System
# Copyright (C) 2014 2015 2016 2017 2018 Nicolas Roche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#=======================================================================

use strict;
use warnings;
use Sys::Syslog qw(:standard :macros);
use Cwd;
use File::Basename;
use File::Temp qw/tempdir/;
use Digest::MD5;
use CGI;
use CGI::Carp qw ( fatalsToBrowser );

$CGI::POST_MAX = 1024 * 1024 * 1024 * 5; # 5 Go
my $safe_filename_characters = "a-zA-Z0-9_.-";
my $uploadDir = "/tmp/somedir";
my $query = CGI->new; # create new CGI object
print $query->header('text/html; charset=utf-8');

my $fd;
my $rc = 0;

###################################################################
# parse options from commande line
# in $1: progam name
# in $2: log facility (local3 or local4)
sub openLog {
    my ($progname, $facility, $severity) = @_;
    my %severities = (
	'emerg' => LOG_EMERG, 
	'alert' => LOG_ALERT, 
	'crit' => LOG_CRIT,
	'err' => LOG_ERR, 
	'warning' => LOG_WARNING, 
	'notice' => LOG_NOTICE, 
	'info', => LOG_INFO,
	'debug' => LOG_DEBUG);
    my $upto;
    
    if (!defined($facility)) {
	$facility = "local2";
    }
    if (!defined($severity)) {
	
	$severity = "info";
    }
    $upto = $severities{$severity};
    if (!defined($upto)) {
	$upto = LOG_DEBUG;
    }
    
    openlog(basename($progname), "pid", $facility);
    setlogmask(LOG_UPTO($upto));
}

# wrapper for syslog
# $1: severity
# $2: formats
# ...: args
sub Log {
    my ($severity, $format, @args) = @_;
    my ($package, $filename, $line) = caller;
    my @severities = ('emerg', 'alert', 'crit', 
		      'err', 'warning', 'notice', 'info', 'debug');
    
    syslog($severity, "[%s %s:%i] $format", 
	   $severities[$severity], basename($filename), $line, @args);
}

sub getCarac
{
    my $label = $_[0];
    my @caracArray;
    my @carac;
    my $caracLabel;
    my $caracValue;
    my $caracNb = 1;
    do {
	$caracLabel = $query->param($label.'Carac'.$caracNb.'Label');
	$caracValue = $query->param($label.'Carac'.$caracNb.'Value');
	$caracNb++;
	
	if ( $caracLabel ) {
	    my @carac = ($caracLabel, $caracValue);
	    push @caracArray, \@carac;
	}
    } while ( $caracLabel );

    return \@caracArray;
}

sub getParent
{
    my $label = $_[0];
    my @parentArray;
    my @parent;
    my $parentLabel;
    my $parentValue;
    my $parentNb = 1;
    do {
	$parentLabel = $query->param($label.'Parent'.$parentNb.'Label');
	$parentValue = $query->param($label.'Parent'.$parentNb.'Value');
	$parentNb++;
	
	if ( $parentLabel ) {
	    my @parent = ($parentLabel, $parentValue);
	    push @parentArray, \@parent;
	}
    } while ( $parentLabel );

    return \@parentArray;
}

sub getPath
{
    my $label = $_[0];
    my $filename = $_[1];
    my $rc = 0;
    my $fd;

    $filename = $query->param($label.'Source');
    if ( !$filename ) {
	Log(LOG_ERR, "%s", "no filename");
	print "no filename";
	goto error;
    }

    # rename uploaded file using safe caracters
    my ( $name, $path, $extension ) = fileparse ( $filename, '..*' );
    $filename = $name . $extension;
    
    $filename =~ tr/ /_/;
    $filename =~ s/[^$safe_filename_characters]//g;
    
    if ( $filename =~ /^([$safe_filename_characters]+)$/ ) {
	$filename = $1;
    }
    else {
	Log(LOG_ERR, "%s", "Filename contains invalid characters");
	print "Filename contains invalid characters";
	goto error;
    }

    # dump uploaded file to /tmp
    my $uploadFileHandler = $query->upload($label.'Source');
    if (! open ($fd, ">", "$uploadDir/$filename")) {
	Log(LOG_ERR, "open '>' fails on '%s': %s", "$uploadDir/$filename", $!);
	print "$!";
	goto error;
    }
    binmode $fd;
    while (<$uploadFileHandler>) {
	print $fd $_;
    }
    close $fd;

    Log(LOG_DEBUG, "upload: %s\n", $filename);
    $_[1] = $filename; # strange but do the job
    $rc = 1;
  error:
    if (!$rc) {
	print "getPath fails"
    }
    return $rc;
}

###################################################################

# open logs
my $facility =  $ENV{'MDTX_LOG_FACILITY'};
my $severity =  $ENV{'MDTX_LOG_SEVERITY_MAIN'}; 
openLog($0, $facility, $severity);
Log(LOG_DEBUG, "%s", "start");

# print header
if (! open ($fd, "<", "../cgiHeader.shtml")) {
    Log(LOG_ERR, "open '<' fails on '%s': %s", "../cgiHeader.shtml", $!);
    print "$!";
    goto error;
}
while ( <$fd> ) {
    print $_;
}
close $fd;

# take care, apache refuse to upload if destination is not owned by www-data.
# (chmod 777 is not enought)
# in fact apache does it but, into a RAM filesystem (not persistent)
my $url= $query->self_url(-relative=>1);
Log(LOG_DEBUG, "url: %s", $url);
my ($mdtx, $coll) = $url =~ /~([^-]+)-([^\/]+)\/cgi\/put\.cgi\?/;
Log(LOG_DEBUG, "coll: %s-%s", $mdtx, $coll);
Log(LOG_DEBUG, "pwd: %s", cwd());
my ($home) = cwd() =~ /^(.*)\/home\/$mdtx-$coll\/public_html\/cgi$/;
Log(LOG_DEBUG, "home: %s", $home);

# create extraction directory
eval {$uploadDir = tempdir(DIR => "$home/tmp/$mdtx-$coll", CLEANUP => 1)};
#eval {$uploadDir = tempdir(DIR => "$home/tmp/$mdtx-$coll")};
if ($@) {
    Log(LOG_ERR, "tempdir fails on '%s': %s", "$home/tmp/$mdtx-$coll", $@);
    goto error;
}

chmod 0755, $uploadDir;

###################################################################

# retrieve categories
my @categoryArray;
my @category;
my $categoryLabel;
my $categoryTop;
my $categoryCarac;
my $categoryParent;
my $categoryNb = 0;
do {
    $categoryNb++;
    $categoryLabel = $query->param('category'.$categoryNb.'Label');
    $categoryTop = $query->param('category'.$categoryNb.'Top');
    $categoryCarac = getCarac('category'.$categoryNb);
    $categoryParent = getParent('category'.$categoryNb);

    if ( $categoryLabel ) {
	my @category = ($categoryLabel, $categoryTop, 
			$categoryCarac, $categoryParent);
	push @categoryArray, \@category;
    }
} while ( $categoryLabel );

# retrieve humans
my @humanArray;
my @human;
my $humanFirstName;
my $humanSecondName;
my $humanRole;
my $humanCarac;
my $humanParent;
my $humanNb = 0;
do {
    $humanNb++;
    $humanFirstName = $query->param('human'.$humanNb.'FirstName');
    $humanSecondName = $query->param('human'.$humanNb.'SecondName');
    $humanRole = $query->param('human'.$humanNb.'Role');
    $humanCarac = getCarac('human'.$humanNb);
    $humanParent = getParent('human'.$humanNb);

    if ( $humanFirstName ) {
	my @human = ($humanFirstName, $humanSecondName, $humanRole, 
		     $humanCarac, $humanParent);
	push @humanArray, \@human;
    }
} while ( $humanFirstName );

# retrieve archives
my @archiveArray;
my @archive;
my $archiveSource;
my $archiveTarget;
my $archiveCarac;
my $archivePath;
my $archiveMd5sum;
my $archiveSize;    
my $archiveNb = 0;

do {
    $archiveNb++;
    $archiveSource = $query->param('archive'.$archiveNb.'Source');
    $archiveTarget = $query->param('archive'.$archiveNb.'Target');
    $archiveCarac = getCarac('archive'.$archiveNb);

    if ( $archiveSource ) {
	if (!getPath('archive'.$archiveNb, $archivePath)) {
	    goto error;
	}
      
	# compute md5sum
	if (! open ($fd, '<', "$uploadDir/$archivePath")) {
	    Log(LOG_ERR, "open '<' fails on %s: %s", "$uploadDir/$archivePath", $!);
	    print "$!";
	    goto error;
	}
	my $ctx = Digest::MD5->new;
	$ctx->addfile($fd);
	$archiveMd5sum = $ctx->hexdigest;
	Log(LOG_DEBUG, "md5sum: %s\n", $archiveMd5sum);

	# get size
	$archiveSize = -s "$uploadDir/$archivePath";
	Log(LOG_DEBUG, "size: %s\n", $archiveSize);

	
	my @archive = ($archivePath, $archiveTarget, $archiveCarac,
	    $archiveMd5sum, $archiveSize);
	push @archiveArray, \@archive;
    }
} while ( $archiveSource );

# retrieve document
my $document=$query->param('document');
my $documentCarac = getCarac('document');
my $documentParent = getParent('document');

# check if all ok
if (!defined($document)) {
    print "No good. Maybe you try to upload more than ".
	($CGI::POST_MAX /1024 /1024) . "Mo.<br>";
    goto error;
}

###################################################################

# serialize catalogNNN.txt
if (! open ($fd, ">", "$uploadDir/catalogNNN.txt")) {
    Log(LOG_ERR, "open '>' fails on '%s': %s", "$uploadDir/catalogNNN.txt", $!);
    print "$!";
    goto error;
}

# print category
foreach my $ref (@categoryArray) {
    my $top = "";
    my $first = 1;
    
    if ( $ref->[1] ) {$top = "Top ";}
    print $fd $top."Category \"".$ref->[0]."\"";

    # print parents
    foreach my $ref2 (@{$ref->[3]}) {
	if ($first) {
	    print $fd ": ";
	}
	else {
	    print $fd ", ";
	}
	print $fd " \"".$ref2->[0]."\"";
	$first = 0;
    }
    print $fd "\n";

    # print caracs
    foreach my $ref2 (@{$ref->[2]}) {
	print $fd " \"".$ref2->[0]."\" = \"".$ref2->[1]."\"\n";
    }
    print $fd "\n";
}

# print humans
foreach my $ref (@humanArray) {
    my $first = 1;

    print $fd "Human\t\"".$ref->[0]."\" \"".$ref->[1]."\"";
    
    # print parents
    foreach my $ref2 (@{$ref->[4]}) {
	if ($first) {
	    print $fd ": ";
	}
	else {
	    print $fd ", ";
	}
	print $fd " \"".$ref2->[0]."\"";
	$first = 0;
    }
    print $fd "\n";

    # print caracs
    foreach my $ref2 (@{$ref->[3]}) {
	print $fd " \"".$ref2->[0]."\" = \"".$ref2->[1]."\"\n";
    }
    print $fd "\n";
}

# print archives
foreach my $ref (@archiveArray) {

    print $fd "Archive\t".$ref->[3].":".$ref->[4]."\n";
    
    # print caracs
    foreach my $ref2 (@{$ref->[2]}) {
	print $fd " \"".$ref2->[0]."\" = \"".$ref2->[1]."\"\n";
    }
    print $fd "\n";
}

# print document
print $fd "Document \"".$document."\"";

# print parents
my $first = 1;
foreach my $ref (@$documentParent) {
    if ($first) {
	print $fd ": ";
    }
    else {
	print $fd ", ";
    }
    print $fd " \"".$ref->[0]."\"";
    $first = 0;
}
print $fd "\n";

foreach my $ref (@humanArray) {
    print $fd " With\t\"".$ref->[2]."\" = \"".$ref->[0]."\" \"".$ref->[1]."\"\n";
}

# print caracs
foreach my $ref (@$documentCarac) {
    print $fd " \"".$ref->[0]."\" = \"".$ref->[1]."\"\n";
}

foreach my $ref (@archiveArray) {
    print $fd " ".$ref->[3].":".$ref->[4]."\n";
}

close $fd;
###################################################################

# display the extract meta-data
print "<pre>";
if (! open ($fd, "<", "$uploadDir/catalogNNN.txt")) {
    Log(LOG_ERR, "open '<' fails on '%s': %s", "uploadDir/catalogNNN.txt", $!);
    print "$!";
    goto error;
}
Log(LOG_INFO, "add to catalog: %s", "uploadDir/catalogNNN.txt");
while ( <$fd> ) {
    print $_;
    Log(LOG_INFO, "> %s", $_);
}
close $fd;

###################################################################

my $parameters = "catalog $uploadDir/catalogNNN.txt";
foreach my $ref (@archiveArray) {
    $parameters .= " file $uploadDir/$ref->[0]";
    if ($ref->[1]) {
	$parameters .= " as $ref->[1]";
    }
}

# call mediatex 
my $shellQuery = "./put.sh $parameters";
Log(LOG_DEBUG, "exec: %s", $shellQuery);
my $shellResult = `$shellQuery`;
if ($?) {
    Log(LOG_ERR, "shell query fails (%i): %s", $?, $shellQuery);
    $rc = 0;
}
else {
    $rc = 1;
}

# display mediatex logs
$shellResult =~ s/\n/<br>/g;
print "<hr>$shellResult</pre><hr>\n";

 error:
    if (! $rc ) {
	Log(LOG_ERR, "%s", "Upload fails (client logs bellow):");
	my @logs = split('<br>', $shellResult);
	foreach my $log (@logs) {
	    Log(LOG_ERR, "> %s", $log);
	}
	print "Upload fails";
}
else {
    Log(LOG_INFO, "%s", "Upload success");
    print "Upload success";
}

# display footers
if (! open ($fd, "<", "../footer.html")) {
    Log(LOG_WARNING, "open '<' fails on '%s': %s", "../footer.html", $!);
}
else {
    while ( <$fd> ) {
	print $_;
    }
    close $fd;
}
