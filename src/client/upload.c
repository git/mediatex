/*=======================================================================
 * Project: MediaTeX
 * Module : upload
 *
 * Manage upload

 MediaTex is an Electronic Records Management System
 Copyright (C) 2014 2015 2016 2017 2018 Nicolas Roche
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 =======================================================================*/

#include "mediatex-config.h"
#include "client/mediatex-client.h"

/*=======================================================================
 * Function   : createUploadFile 
 * Description: Create, by memory allocation an image file (ISO file)
 * Synopsis   : UploadFile* createUploadFile(void)
 * Input      : N/A
 * Output     : The address of the create empty image file
 =======================================================================*/
UploadFile* 
createUploadFile(void)
{
  UploadFile* rc = 0;

  rc = (UploadFile*)malloc(sizeof(UploadFile));
  if(rc == 0) goto error;
   
  memset(rc, 0, sizeof(UploadFile));

  return(rc);
 error:
  logMemory(LOG_ERR, "malloc: cannot create an UploadFile");
  return(rc);
}

/*=======================================================================
 * Function   : destroyUploadFile 
 * Description: Destroy an image file by freeing all the allocate memory.
 * Synopsis   : void destroyUploadFile(UploadFile* self)
 * Input      : UploadFile* self = the address of the image file to
 *              destroy.
 * Output     : Nil address of an image FILE
 =======================================================================*/
UploadFile* 
destroyUploadFile(UploadFile* self)
{
  if(self == 0) goto error;
  destroyString(self->source);
  destroyString(self->target);
  free(self);
 error:
  return (UploadFile*)0;
}

/*=======================================================================
 * Function   : uploadExtract
 * Description: parse uploaded extract file
 * Synopsis   : static int uploadExtract(Collection* upload, char* path)
 * Input      : Collection* upload: collection used to parse upload
 *              char* path: input extract file to parse
 * Output     : TRUE on success
 =======================================================================*/
static int 
uploadExtract(Collection* upload, char* path)
{ 
  int rc = FALSE;
  ExtractTree* self = 0;

  logMain(LOG_DEBUG, "uploadExtract");
  checkCollection(upload);
  checkLabel(path);
  
  if (!(upload->extractTree = createExtractTree())) goto error;
  if (!(upload->extractDB = createString(path))) goto error;
  if (!parseExtractFile(upload, path)) {
    logMain(LOG_ERR, "fails to load uploaded extract catalog");
    goto error;
  }

  // assert there is no INC content  
  if (!(self = upload->extractTree)) goto error;
  if (avl_count(self->inc->childs) > 0) {
    logMain(LOG_ERR, "unexpected incoming rules on upload");
    rc = FALSE;
  }

  rc = TRUE;
 error:
  if (!rc) {
    logMain(LOG_ERR, "uploadExtract fails");
  }
  upload->extractDB = destroyString(upload->extractDB);
  return rc;
}

/*=======================================================================
 * Function   : uploadContent
 * Description: add uploaded content file to extract metadata
 * Synopsis   : static Archive* uploadContent(
 *                               Collection* coll, UploadFile* upFile)
 * Input      : Collection* coll: collection used to add content
 *              char* path: input file to upload
 * Output     : Archive object on success ; NULL on error
 =======================================================================*/
static Archive* 
uploadContent(Collection* coll, UploadFile* upFile)
{ 
  Archive* rc = 0;
  struct stat statBuffer;
  CheckData md5; 
  Archive* archive = 0;
  time_t time = 0;
  struct tm date;
  char dateString[32];
  Container* container = 0;
  char* path = 0;
  char* target = 0;
  char* ptr = 0;

  logMain(LOG_DEBUG, "uploadContent");
  checkCollection(coll);
  path = upFile->source;
  checkLabel(path);

  if (!coll->extractTree) {
    if (!(coll->extractTree = createExtractTree())) goto error;
  }
  
  // get file attributes (size)
  if (stat(path, &statBuffer)) {
    logMain(LOG_ERR, "status error on %s: %s", path, strerror(errno));
    goto error;
  }

  // compute hash
  memset(&md5, 0, sizeof(CheckData));
  md5.path = path;
  md5.size = statBuffer.st_size;
  md5.opp = CHECK_CACHE_ID;
  if (!doChecksum(&md5)) goto error;

  // archive to upload
  if (!(archive = addArchive(coll, md5.fullMd5sum, statBuffer.st_size)))
    goto error;

  // add incoming extraction rule
  if (!(time = currentTime())) goto error;
  if (localtime_r(&time, &date) == (struct tm*)0) {
    logMemory(LOG_ERR, "localtime_r returns on error");
    goto error;
  }

  sprintf(dateString, "%04i-%02i-%02i,%02i:%02i:%02i", 
	  date.tm_year + 1900, date.tm_mon+1, date.tm_mday,
	  date.tm_hour, date.tm_min, date.tm_sec);

  if (!(container = coll->extractTree->inc)) goto error;
  if (!(addFromAsso(container, archive, dateString))) 
    goto error;

  // add extraction rule
  if (upFile->target) {
    if (!(target = createString(upFile->target))) goto error;
    
    // if target is a directory
    if (target[strlen(target)-1] == '/') {

      // concat the filename
      for (ptr = path+strlen(path)-1; ptr > path && *ptr != '/'; --ptr);
      if (!(target = catString(target, ptr+1))) goto error;
    }
    
    if (!(container = coll->extractTree->img)) goto error;
    if (!(addFromAsso(container, archive, target)))
      goto error;
  }
  
  rc = archive;
  archive = 0;
 error:
  if (!rc) {
    logMain(LOG_ERR, "uploadExtract fails");
  }
  if (target) destroyString(target);
  return rc;
}

/*=======================================================================
 * Function   : isFileRefbyExtract
 * Description: check extract provides a container describing the data
 * Synopsis   : int isFileRefbyExtract(Collection *coll, 
 *                                     Archive* archive)
 * Input      : Collection* coll
 *              Archive* archive
 * Output     : TRUE on success
 =======================================================================*/
int 
isFileRefbyExtract(Collection *coll, Archive* archive)
{ 
  int rc = FALSE;

  logMain(LOG_DEBUG, "isFileRefbyExtract");
  checkCollection(coll);
  
  if (!archive->toContainer) {
    logMain(LOG_ERR, 
	    "Extract file should provide a container for %s%lli",
	    archive->hash, archive->size);
    goto error;
  }

  rc = TRUE;
 error:
  if (!rc) {
    logMain(LOG_ERR, "isFileRefbyExtract fails");
  }
  return rc;
}

/*=======================================================================
 * Function   : areAlreadyThere
 * Description: assert extract refers to known archives
 * Synopsis   : int areAlreadyThere(Collection *coll,
 *                                  Collection *upload)
 * Input      : Collection* coll:   actuals extract rules
 *              Collection* upload: new extract rules
 * Output     : TRUE on success
 =======================================================================*/
int
areAlreadyThere(Collection *coll, Collection* upload)
{
  int rc = FALSE;
  ExtractTree* self = 0;
  Archive* archUp = 0;
  Archive* archCol = 0;
  Container* container = 0;
  AVLNode *node = 0;
  RGIT* curr = 0;

  logMain(LOG_DEBUG, "areAlreadyThere");
  checkCollection(coll);
  checkCollection(upload);
  if (!(self = upload->extractTree)) goto error;
  rc = TRUE;
  
  // loop on uploaded containers
  for(node = self->containers->head; node; node = node->next) {
    container = node->item;
    curr = 0;

    // assert parents containers are already into the extractTree
    while ((archUp = rgNext_r(container->parents, &curr))) {
      if (!(archCol = getArchive(coll, archUp->hash, archUp->size))) {
	  logMain(LOG_ERR, "unknown container %s:%lli", 
		  archUp->hash, archUp->size);
	  rc = FALSE;
      }
    }
  }

 error:
  if (!rc) {
    logMain(LOG_ERR, "areAlreadyThere fails");
  }
  return rc;
}

/*=======================================================================
 * Function   : uploadFile 
 * Description: Ask daemon to upload the files
 * Synopsis   : static int
 *               uploadFile(Collection* coll, RG* upFiles)
 * Input      : Collection* coll
 *              RG* upFiles: UploadFile's ring (source, target + archive)
 * Output     : TRUE on success
 =======================================================================*/
static int
uploadFile(Collection* coll, RG* upFiles)
{
  int rc = FALSE;
  int socket = -1;
  RecordTree* tree = 0;
  Record* record = 0;
  UploadFile* upFile = 0;
  Archive* archive = 0;
  char* source = 0;
  char* target = 0;
  char buf[MAX_SIZE_STRING] = "";
  char* extra = 0;
  char* message = 0;
  char reply[576];
  int status = 0;
  int n = 0;

  logMain(LOG_DEBUG, "uploadFile");
  checkCollection(coll);
  if (isEmptyRing(upFiles)) {
    logMain(LOG_ERR, "Please provide a ring");
    goto error;
  }

  // build the UPLOAD message
  if (!getLocalHost(coll)) goto error;
  if (!(tree = createRecordTree())) goto error; 
  tree->collection = coll;
  tree->messageType = UPLOAD;
  //tree->doCypher = FALSE;

  // add files to the UPLOAD message
  rgRewind(upFiles);
  while ((upFile = rgNext(upFiles))) {
    source = upFile->source;
    target = upFile->target;
    archive = upFile->archive;
  
    // tels "/source:target"
    if (!(extra = getAbsolutePath(source))) goto error;
    if (snprintf(buf, MAX_SIZE_STRING, "%s%s%s", extra, 
		 target?":":"", target?target:"") 
	>= MAX_SIZE_STRING) {
      logMain(LOG_ERR, "buffer too few to copy source and target paths");
      goto error;
    }

    extra = destroyString(extra);
    extra = createString(buf);
    if (!(record = addRecord(coll, coll->localhost, archive,
			     SUPPLY, extra))) goto error;
    extra = 0;
    if (!avl_insert(tree->records, record)) {
      logMain(LOG_ERR, "cannot add record to tree (already there?)");
      goto error;
    }
    record = 0;
  }
  
  // ask daemon to upload the file into the cache
  if ((socket = connectServer(coll->localhost)) == -1) goto error;
  if (!upgradeServer(socket, tree, 0)) goto error;
    
  // read reply
  if (env.dryRun) goto end;
  if ((n = tcpRead(socket, reply, 255)) > 0) {
    reply[n-1] = (char)0; // remove ending \n
  }
  if (sscanf(reply, "%i", &status) < 1) {
    logMain(LOG_ERR, "error parsing daemon reply: %s", reply);
    goto error;
  }
  message = strstr(reply, " ");
    
  if (status != 210) {
    logMain(LOG_ERR, "daemon says (%i)%s", status, message);
    goto error;
  }

  logMain(LOG_INFO, "daemon says (%i)%s", status, message);
 end:
  rc = TRUE;
 error:
  if (!rc) {
    logMain(LOG_ERR, "uploadFile fails");
  }
  extra = destroyString(extra);
  if (record) delRecord(coll, record);
  tree = destroyRecordTree(tree);
  if (!env.dryRun && socket != -1) close(socket);
  return rc;
}


/*=======================================================================
 * Function   : mdtxUpload
 * Description: upload manager
 * Synopsis   : int mdtxUpload(char* label, char* catalog, char* extract, 
 *                             RG* upFiles)
 * Input      : char* label: related collection
 *              char* catalog: catalog metadata file to upload
 *              char* extract: extract metadata file to upload
 *              RG* upFiles: ring of UploadFile* (source+target paths)
 * Output     : TRUE on success
 =======================================================================*/
int 
mdtxUpload(char* label, char* catalog, char* extract, RG* upFiles)
{ 
  int rc = FALSE;
  Collection *coll = 0;
  Collection *upload = 0;
  UploadFile* upFile = 0;
  
  logMain(LOG_DEBUG, "mdtxUpload");
  checkLabel(label);
  if (!catalog && !extract && isEmptyRing(upFiles)) {
    logMain(LOG_ERR, "please provide some content to upload");
    goto error;
  }
  if (!(coll = mdtxGetCollection(label))) goto error;

  // create a new collection to parse upload contents
  if ((upload = getCollection("_upload_"))) {
    logMain(LOG_ERR, "internal '_upload_' collection already used");
    goto error;
  }
  if (!(upload = addCollection("_upload_"))) goto error;
  if (!(upload->masterLabel = createString(coll->masterUser))) goto error;
  if (!(upload->user = createString(coll->user))) goto error;
  strcpy(upload->masterHost, DEFAULT_HOST);

  // assert there is no INC content (internal use only) 
  if (extract) {
    if (!uploadExtract(upload, extract)) goto error;
    if (avl_count(upload->extractTree->inc->childs) > 0) {
      logMain(LOG_ERR, "uploaded extract catalog contains incoming rules");
      goto error;
    }
  }    

  // if both extract catalog and extract files are provided, 
  // we guess user wants to provide rules for all incoming files.
  if (extract && !isEmptyRing(upFiles)) {
    rgRewind(upFiles);
    while ((upFile = rgNext(upFiles))) {
      if (!(upFile->archive = uploadContent(upload, upFile))) goto error;
      if (!isFileRefbyExtract(upload, upFile->archive)) goto error;
      upFile->archive = 0;
    }
  }

  // check archive's consistency while concatenating catalogs
  // note: catalog and extract may refer to images too
  if (!loadCollection(coll, EXTR|SERV)) goto error;

  // add rules for incoming files (and get archives for uploadFile)
  if (!isEmptyRing(upFiles)) {
    while ((upFile = rgNext(upFiles))) {
      // incoming files (alones) may be uploaded several times,
      // if the daemon do not already old them as local supply
      // (usefull if archive was lost for instance)
      if (!(upFile->archive = uploadContent(coll, upFile)))
	goto error;
    }
  }
  
  // extract rules file may be provided without incoming files
  // (to add forgotten container's rules for instance).
  // here we assert that it does not introduce unknown containers
  if (extract && !areAlreadyThere(coll, upload)) {
    logMain(LOG_ERR, "uploaded extract catalog refers to unknown containers");
    goto error;
  }

  // check new extract rules does not overrides current ones 
  // (incoming are merged)
  if (extract) {
    // concatenate new extract file
    if (!parseExtractFile(coll, extract)) {
      logMain(LOG_ERR, "uploaded extract catalog overrides existing rules");
      goto error;
    }
  }

  // and check new catalog does not links to unknown archives
  if (catalog) {
    if (!loadCollection(coll, CTLG)) goto error;
    if (!parseCatalogFile(coll, catalog)) {
      logMain(LOG_ERR, "fails to upload new catalog."
	      " Maybe it refers to unknown archives");
      goto error;
    }
  }
  
  // ask daemon to upload the file
  if (!isEmptyRing(upFiles)) {
    if (!uploadFile(coll, upFiles)) goto error;
  }
  
  // save changes
  if (catalog) {
    if (!wasModifiedCollection(coll, CTLG)) goto error;
    if (!releaseCollection(coll, CTLG)) goto error;
  }
  if (extract || !isEmptyRing(upFiles)) {
    if (!wasModifiedCollection(coll, EXTR)) goto error;
  }
  if (!releaseCollection(coll, EXTR|SERV)) goto error;
  rc = TRUE;
 error:
  if (!rc) {
    logMain(LOG_ERR, "mdtxUpload fails");
  }
  delCollection(upload);
  return rc;
}

/* Local Variables: */
/* mode: c */
/* mode: font-lock */
/* mode: auto-fill */
/* End: */
