/*=======================================================================
 * Project: MediaTeX
 * Module : extractHtml
 *
 * HTML extraction catalog serializer

 MediaTex is an Electronic Records Management System
 Copyright (C) 2014 2015 2016 2017 2018 Nicolas Roche
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
 =======================================================================*/

#include "mediatex-config.h"
#include "client/mediatex-client.h"

/*=======================================================================
 * Function   : getArchiveListUri
 * Description: Get relative path to archive list page
 * Synopsis   : int getArchiveListUri(char* buf, char* path, int id) 
 *              char* path = path to use as prefix
 *              int id = number use to compute page location
 * Output     : char* buf = the Uri
 *              TRUE on success
 =======================================================================*/
static 
int getArchiveListUri(char* buf, char* path, int id) 
{
  int rc = FALSE;

  rc = getRelativeUri(buf, path, "lists");
  if (rc && id > 0) rc = getListUri(buf + strlen(buf), id);

  return rc;
}

/*=======================================================================
 * Function   : getBadListUri
 * Description: Get relative path to bad score archive's list page
 * Synopsis   : int getBadListUri(char* buf, char* path, int id) 
 *              char* path = path to use as prefix
 *              int id = number use to compute page location
 * Output     : char* buf = the Uri
 *              TRUE on success
 =======================================================================*/
static 
int getBadListUri(char* buf, char* path, int id) 
{
  int rc = FALSE;

  rc = getRelativeUri(buf, path, "badLists");
  if (rc && id > 0) rc = getListUri(buf + strlen(buf), id);

  return rc;
}

/*=======================================================================
 * Function   : getBadListUri
 * Description: Get relative path to images's list page
 * Synopsis   : int getImageListUri(char* buf, char* path, int id) 
 *              char* path = path to use as prefix
 *              int id = number use to compute page location
 * Output     : char* buf = the Uri
 *              TRUE on success
 =======================================================================*/
static 
int getImageListUri(char* buf, char* path, int id) 
{
  int rc = FALSE;

  rc = getRelativeUri(buf, path, "imageLists");
  if (rc && id > 0) rc = getListUri(buf + strlen(buf), id);

  return rc;
}

/*=======================================================================
 * Function   : getLocationListUri
 * Description: Get relative path to location's list page
 * Synopsis   : int getLocationListUri(char* buf, char* path, int id) 
 *              char* path = path to use as prefix
 *              int id = number use to compute page location
 * Output     : char* buf = the Uri
 *              TRUE on success
 =======================================================================*/
static 
int getLocationListUri(char* buf, char* path, int id) 
{
  int rc = FALSE;

  rc = getRelativeUri(buf, path, "locationLists");
  if (rc && id > 0) rc = getListUri(buf + strlen(buf), id);

  return rc;
}

/*=======================================================================
 * Function   : htmlFromAsso
 * Description: HTML for FromAsso.
 * Synopsis   : static int htmlFromAsso(
 *                               FromAsso* self, FILE* fd, int isHeader)
 * Input      : FromAsso* self: what to latexalize
 *              FILE* fd: where to latexalize
 *              int isHeader:
 * Output     : TRUE on success
 =======================================================================*/
static int 
htmlFromAsso(FromAsso* self, FILE* fd, int isHeader)
{
  int rc = FALSE;
  Container* container = 0;
  Archive* archive = 0;
  int many = FALSE;
  char url[64];
  char label[16];
  int i = 0;

  if(self == 0) goto error;
  logMain(LOG_DEBUG, "htmlFromAsso");

  container = self->container;
  many = (container->parents->nbItems > 1);
  htmlLiOpen(fd);

  htmlItalic(fd, self->path?self->path:"??");
  if (!fprintf(fd, _(" from %s "), strEType(container->type))) goto error;
  if (many) fprintf(fd, "%s\n", getContainerScore(container));

  // do not sort !
  if (many) htmlUlOpen(fd);
  
  while ((archive = rgNext(container->parents))) {
    if (isHeader) {
      getArchiveUri(url, "../../../..", archive);
    } else {
      getArchiveUri(url, "../..", archive);
    }

    if (many) htmlLiOpen(fd);

    if (many) {
      if (!sprintf(label, _("part %i "), ++i)) goto error;
      htmlItalic(fd, label);
    }

    htmlLink(fd, 0, url, getArchiveScore(archive));    
    if (many) htmlLiClose(fd);
  }

  if (many) htmlUlClose(fd);
  htmlLiClose(fd); 
  if (!fprintf(fd, "\n")) goto error;

  rc = TRUE;
 error:
  if (!rc) {
    logMain(LOG_ERR, "htmlFromAsso fails");
  }
  return rc;
}

/*=======================================================================
 * Function   : htmlLocationAsso
 * Description: HTML for LocationAsso.
 * Synopsis   : static int serializeHtmlLocationAsso(Collection* coll,
 *                           LocationAsso* self, FILE* fd, int isHeader)
 * Input      : LocationAsso* self: what to latexalize
 *              Collection* coll
 *              FILE* fd: where to latexalize
 *              int isHeader:
 * Output     : TRUE on success
 =======================================================================*/
static int 
htmlLocationAsso(Collection* coll, LocationAsso* self,
		 FILE* fd, int isHeader)
{
  int rc = FALSE;
  Location* location = 0;
  Server* server = 0;
  char url[64];
  char text[640];

  if(self == 0) goto error;
  logMain(LOG_DEBUG, "htmlLocationAsso");
  location = self->location;
  
  htmlLiOpen(fd);
  htmlItalic(fd, self->path?self->path:"??");
  if (!fprintf(fd, _(" from "))) goto error;
  
  if ((strcmp(location->fingerPrint, "ALL"))) {
    if (!(server = getServer(coll, location->fingerPrint))) {
      logMain(LOG_ERR, "fails to get server %s", location->fingerPrint);
      goto error;
    }
    if (!sprintf(text, "%s %s:%s", strLType(location->type),
		 server->host, location->uri)) goto error;
  }
  else {    
    if (!sprintf(text, "%s ALL:%s",
		 strLType(location->type), location->uri)) goto error;
  }
  
  // do not sort !
  if (isHeader) {
    getLocationUri(url, "../../../..", location);
  } else {
    getLocationUri(url, "../..", location);
  }

  htmlLink(fd, 0, url, text);    
  htmlLiClose(fd); 
  if (!fprintf(fd, "\n")) goto error;

  rc = TRUE;
 error:
  if (!rc) {
    logMain(LOG_ERR, "htmlLocationAsso fails");
  }
  return rc;
}


/*=======================================================================
 * Function   : htmlContainer
 * Description: HTML for Container.
 * Synopsis   : int htmlContainer(Container* self, FILE* fd)
 * Input      : Container* self: what to latexalize
 *              FILE* fd: where to latexalize
 * Output     : TRUE on success
 =======================================================================*/
static int
htmlContainer(Container* self, FILE* fd)
{
  int rc = FALSE;
  FromAsso* asso = 0;
  AVLNode *node = 0;
  int many = FALSE;
  char url [128];

  if(self == 0) goto error;
  logMain(LOG_DEBUG, "htmlContainer");

  many = (avl_count(self->childs) > 1);
  htmlLiOpen(fd);

  fprintf(fd, "%s ", strEType(self->type));
  if (many) fprintf(fd, "%s", getContainerScore(self));
  if (many) htmlUlOpen(fd);

  for(node = self->childs->head; node; node = node->next) {
    asso = node->item;

    getArchiveUri(url, "../..", asso->archive);
    if (many) htmlLiOpen(fd);
    htmlLink(fd, 0, url, getArchiveScore(asso->archive));
    if (!fprintf(fd, "%s", " ")) goto error;
    htmlItalic(fd, asso->path);
    if (many) htmlLiClose(fd);
  }

  if (many) htmlUlClose(fd);
  htmlLiClose(fd);
  if (!fprintf(fd, "\n")) goto error;

  rc = TRUE;
 error:
  if (!rc) {
    logMain(LOG_ERR, "htmlContainer fails");
  }
  return rc;
}

/*=======================================================================
 * Function   : serializeHtmlArchiveContentList
 * Description: Latexalize an archive content list.
 * Synopsis   : int serializeHtmlArchiveContentList(Collection* coll,
 *                                                       int i, int n)
 * Input      : Collection* coll
 *              int i = current archive list
 *              int n = last archive list
 * Output     : TRUE on success
 =======================================================================*/
static int
serializeHtmlArchiveContentList(Collection* coll, AVLNode **node,
			 int archId, int i, int n)
{
  int rc = FALSE;
  FromAsso* asso = 0;
  Archive* archive = 0;
  FILE *fd = stdout;
  char *path = 0;
  char url[128];
  char text[128];
  int j = 0;

  getArchiveContentListUri(url, "/", archId, i);
  if (!(path = createString(coll->htmlScoreDir))) goto error;
  if (!(path = catString(path, url))) goto error;

  logMain(LOG_DEBUG, "serialize %s", path);
  if (!env.dryRun && (fd = fopen(path, "w")) == 0) {
    logMain(LOG_ERR, "fopen %s fails: %s", path, strerror(errno));
    goto error;
  }

  fprintf(fd, "<!--#include virtual='../header.shtml' -->");
  htmlPOpen(fd);
  htmlUlOpen(fd);

  if (!serializeHtmlListBar(coll, fd, i, n)) goto error;

  for (j = 0; j < MAX_INDEX_PER_PAGE && *node; ++j) {
    asso = (*node)->item;
    archive = asso->archive; // the content

    getArchiveUri(url, "../../../..", archive);
    if (!sprintf(text, "%s:%lli ", archive->hash,
		 (long long int)archive->size)) goto error;

    htmlLiOpen(fd);
    htmlVerb(fd, text);
    htmlLink(fd, 0, url, getArchiveScore(archive));
    htmlLiClose(fd);

    *node = (*node)->next;
  }

  if (!serializeHtmlListBar(coll, fd, i, n)) goto error;

  htmlUlClose(fd);
  htmlPClose(fd);
  htmlSSIFooter(fd, "../../../../..");

  ++env.progBar.cur;
  rc = TRUE;
  if (!env.dryRun) {
    fclose(fd);
  } else {
    fflush(stdout);
  }
 error:
  if (!rc) {
    logMain(LOG_ERR, "serializeHtmlArchiveContentList fails");
  }
  path = destroyString(path);
  return rc;
}


/*=======================================================================
 * Function   : serializeHtmlScoreArchive
 * Description: Latexalize a Archive.
 * Synopsis   : int serializeHtmlArchive(Collection* coll, Archive* self)
 * Input      : Collection* coll
 *              Archive* self = what to latexalize
 * Output     : TRUE on success
 =======================================================================*/
static int 
serializeHtmlScoreArchive(Collection* coll, Archive* self)
{
  int rc = FALSE;
  Configuration* conf = 0;
  AssoCarac *assoCarac = 0;
  Document* document = 0;
  FromAsso* asso = 0;
  LocationAsso* locAsso = 0;
  Image* image = 0;
  AVLNode *node = 0;
  FILE *fd = stdout;
  char *path = 0;
  char url[512];
  char text[128];
  int isHeader = FALSE;
  char mainHome[] = "../..";
  char headerHome[] = "../../../..";
  char* home = mainHome;
  int nb = 0, n = 0, i = 0;
  struct tm date;

  checkArchive(self);
  if (!(conf = getConfiguration())) goto error;

  // have to manage 2 cases :
  // - when use as main page
  // - when included from content list as header

  // if we need lists for the contents or not
  isHeader = (self->toContainer &&
  	      (nb = avl_count(self->toContainer->childs))
  	      > MAX_INDEX_PER_PAGE);

  if (isHeader) {
    home = headerHome;

    // make dir archives/0000/000
    getArchiveUri2(url, "/", self->id);
    if (!(path = createString(coll->htmlScoreDir))) goto error;
    if (!(path = catString(path, url))) goto error;
    if (mkdir(path, 0755)) {
      if (errno != EEXIST) {
    	logMain(LOG_ERR, "mkdir fails: %s", strerror(errno));
    	goto error;
      }
    }
  }
  
  // serialize the archive shtml file. May be
  // - archives/0000/000.shtml or
  // - archives/0000/000/header.shtml
  if (!isHeader) {
    getArchiveUri1(url, "/", self->id);
  } else {
    getArchiveUri2(url, "/", self->id);
    sprintf(url + strlen(url), "%s", "/header.shtml");
    path = destroyString(path);
  }
  if (!(path = createString(coll->htmlScoreDir))) goto error;
  if (!(path = catString(path, url))) goto error;
  logMain(LOG_DEBUG, "serialize %s", path);
  if (!env.dryRun && (fd = fopen(path, "w")) == 0) {
    logMain(LOG_ERR, "fopen %s fails: %s", path, strerror(errno));
    goto error;
  }
  
  if (isHeader) {
    htmlSSIHeader2(fd, "../../../..", "score");
  } else {
    htmlSSIHeader(fd, "../../..", "score");
  }

  if (!sprintf(url,
  	       "%s/../cgi/get.cgi?hash=%s&size=%lli", home,
  	       self->hash, (long long int)self->size)) goto error;

  if (!sprintf(text, "%s:%lli", self->hash, (long long int)self->size))
    goto error;

  htmlPOpen(fd);
  htmlBold(fd, _("Record:"));
  htmlVerb(fd, text);
  htmlBr(fd);
  if (!sprintf(text, "%s/../icons", home)) goto error;
  htmlImage(fd, text, "floppy-icon.png", url);
  if (!fprintf(fd, " %s", getArchiveScore(self))) goto error;

  /* latexalize caracs */
  if (!isEmptyRing(self->assoCaracs)) {
    rgRewind(self->assoCaracs);
    htmlUlOpen(fd);

    while ((assoCarac = rgNext(self->assoCaracs))) {
      if (!htmlAssoCarac(fd, assoCarac)) goto error;
    }

    htmlUlClose(fd);
  }
  htmlPClose(fd);

  // catalog
  htmlPOpen(fd);
  if (!fprintf(fd,
  	       _("\nCatalog reference as documents:\n")))
    goto error;
  htmlUlOpen(fd);
  if (!isEmptyRing(self->documents)) {
    if (!rgSort(self->documents, cmpDocument)) goto error;
    while ((document = rgNext(self->documents))) {
      if (!sprintf(text, "%s/../index", home)) goto error;
      getDocumentUri(url, text, document->id);

      htmlLiOpen(fd);
      htmlLink(fd, 0, url, document->label);
      htmlLiClose(fd);
    }
  }
  else {
    htmlLiOpen(fd);
    if (!fprintf(fd, _("Not in catalog\n"))) goto error;
    htmlLiClose(fd);
  }
  htmlUlClose(fd);
  htmlPClose(fd);

  // servers
  htmlPOpen(fd);
  if (!fprintf(fd, _("\nProvided by servers:\n"))) goto error;
  htmlUlOpen(fd);
  if (!isEmptyRing(self->images)) {
    if (!rgSort(self->images, cmpImage)) goto error;
    while ((image = rgNext(self->images))) {
      if (!sprintf(text, "%s/servers/srv_%s.shtml", home,
  		   image->server->fingerPrint)) goto error;

      htmlLiOpen(fd);
      htmlLink(fd, 0, text, image->server->host);
      if (!fprintf(fd, " (%.2f)", image->score)) goto error;
      htmlLiClose(fd);
    }
  }
  else {
    htmlLiOpen(fd);
    if (!fprintf(fd, _("Is not an image\n"))) goto error;
    htmlLiClose(fd);
  }
  htmlUlClose(fd);
  htmlPClose(fd);
 
  // from
  htmlPOpen(fd);
  if (!fprintf(fd, _("\nComes from:\n"))) goto error;
  htmlUlOpen(fd);
  if (!isEmptyRing(self->fromContainers)) {
    // already sorted (do not remind why)
    //if (!rgSort(self->fromContainers, cmpContainer)) goto error;
    rgRewind(self->fromContainers);
    while ((asso = rgNext(self->fromContainers))) {
      htmlFromAsso(asso, fd, isHeader);
    }
  }
  else {
    if (self->uploadTime) {
      if (localtime_r(&self->uploadTime, &date) == (struct tm*)0) {
	logMemory(LOG_ERR, "localtime_r returns on error");
	goto error;
      }
      
      htmlLiOpen(fd);
      if (!fprintf(fd, _("Cache: uploaded on "))) goto error;
      if (!fprintf(fd, "%04i-%02i-%02i %02i:%02i:%02i",
	  date.tm_year + 1900, date.tm_mon+1, date.tm_mday,
		   date.tm_hour, date.tm_min, date.tm_sec)) goto error;
      htmlLiClose(fd);
    }
    else {
      htmlLiOpen(fd);
      if (!fprintf(fd, _("External source"))) goto error;
      htmlLiClose(fd);
    }
  }
  if (!isEmptyRing(self->fromLocations)) {
    // already sorted ?
    rgRewind(self->fromLocations );
    while ((locAsso = rgNext(self->fromLocations))) {
      htmlLocationAsso(coll, locAsso, fd, isHeader);
    }
  }
  htmlUlClose(fd);
  htmlPClose(fd);

  // to
  htmlPOpen(fd);
  if (!fprintf(fd, _("\nContent: %i\n"), nb)) goto error;
  htmlUlOpen(fd);

  if (isHeader) {
    fprintf(fd, "%s ", strEType(self->toContainer->type));
    fprintf(fd, "%s", getContainerScore(self->toContainer));
    
    // - when included from content list as header
    path = destroyString(path);
    getArchiveUri2(url, "/", self->id);
    if (!(path = createString(coll->htmlScoreDir))) goto error;
    if (!(path = catString(path, url))) goto error;

    // make content list directories: archive/0000/000/0000
    n = (nb - 1) / MAX_INDEX_PER_PAGE +1;
    if (!htmlMakeDirs(path, n)) goto error;
    
    // serialize content lists
    node = self->toContainer->childs->head;
    for (i=1 ; i<=n; ++i) {
      if (!serializeHtmlArchiveContentList(coll, &node, self->id, i, n))
  	goto error;
    }
  }
  else {
    // - when use as main page
    if (self->toContainer) {
      htmlContainer(self->toContainer, fd);
    } else {
      htmlLiOpen(fd);
      if (!fprintf(fd, _("No content\n"))) goto error;
      htmlLiClose(fd);
    }
  }

  htmlUlClose(fd);
  htmlPClose(fd);

  if (!isHeader) {
    htmlSSIFooter(fd, "../../..");
  }
  
  ++env.progBar.cur;
  rc = TRUE;
  if (!env.dryRun) {
    fclose(fd);
  } else {
    fflush(stdout);
  }
 error:
  if (!rc) {
    logMain(LOG_ERR, "serializeHtmlScoreArchive fails");
  }
  path = destroyString(path);
  return rc;
}


/*=======================================================================
 * Function   : htmlLocation
 * Description: HTML for location.
 * Synopsis   : int htmlLocation(Location* self, FILE* fd)
 * Input      : Location* self: what to latexalize
 *              FILE* fd: where to latexalize
 * Output     : TRUE on success
 =======================================================================*/
static int
htmlLocation(Location* self, FILE* fd)
{
  int rc = FALSE;
  LocationAsso* asso = 0;
  AVLNode *node = 0;
  int many = FALSE;
  char url [128];

  if(self == 0) goto error;
  logMain(LOG_DEBUG, "htmllocation");

  many = (avl_count(self->childs) > 1);
  htmlLiOpen(fd);

  fprintf(fd, "%s ", strLType(self->type));
  if (many) htmlUlOpen(fd);

  for(node = self->childs->head; node; node = node->next) {
    asso = node->item;

    getArchiveUri(url, "../..", asso->archive);
    if (many) htmlLiOpen(fd);
    htmlLink(fd, 0, url, getArchiveScore(asso->archive));
    if (!fprintf(fd, "%s", " ")) goto error;
    htmlItalic(fd, asso->path);
    if (many) htmlLiClose(fd);
  }

  if (many) htmlUlClose(fd);
  htmlLiClose(fd);
  if (!fprintf(fd, "\n")) goto error;

  rc = TRUE;
 error:
  if (!rc) {
    logMain(LOG_ERR, "htmlLocation fails");
  }
  return rc;
}

/*=======================================================================
 * Function   : serializeHtmlLocationContentList
 * Description: Latexalize an location content list.
 * Synopsis   : int serializeHtmlLocationContentList(Collection* coll,
 *                                                       int i, int n)
 * Input      : Collection* coll
 *              int i = current location list
 *              int n = last location list
 * Output     : TRUE on success
 =======================================================================*/
static int
serializeHtmlLocationContentList(Collection* coll, AVLNode **node,
			 int archId, int i, int n)
{
  int rc = FALSE;
  LocationAsso* asso = 0;
  Archive* archive = 0;
  FILE *fd = stdout;
  char *path = 0;
  char url[128];
  char text[128];
  int j = 0;

  getLocationContentListUri(url, "/", archId, i);
  if (!(path = createString(coll->htmlScoreDir))) goto error;
  if (!(path = catString(path, url))) goto error;

  logMain(LOG_DEBUG, "serialize %s", path);
  if (!env.dryRun && (fd = fopen(path, "w")) == 0) {
    logMain(LOG_ERR, "fopen %s fails: %s", path, strerror(errno));
    goto error;
  }

  fprintf(fd, "<!--#include virtual='../header.shtml' -->");
  htmlPOpen(fd);
  htmlUlOpen(fd);

  if (!serializeHtmlListBar(coll, fd, i, n)) goto error;

  for (j = 0; j < MAX_INDEX_PER_PAGE && *node; ++j) {
    asso = (*node)->item;
    archive = asso->archive; // the content

    getArchiveUri(url, "../../../..", archive);
    if (!sprintf(text, "%s:%lli ", archive->hash,
		 (long long int)archive->size)) goto error;

    htmlLiOpen(fd);
    htmlVerb(fd, text);
    htmlLink(fd, 0, url, getArchiveScore(archive));
    htmlLiClose(fd);

    *node = (*node)->next;
  }

  if (!serializeHtmlListBar(coll, fd, i, n)) goto error;

  htmlUlClose(fd);
  htmlPClose(fd);
  htmlSSIFooter(fd, "../../../../..");

  ++env.progBar.cur;
  rc = TRUE;
  if (!env.dryRun) {
    fclose(fd);
  } else {
    fflush(stdout);
  }
 error:
  if (!rc) {
    logMain(LOG_ERR, "serializeHtmlLocationContentList fails");
  }
  path = destroyString(path);
  return rc;
}


/*=======================================================================
 * Function   : serializeHtmlLocation
 * Description: Latexalize a Location.
 * Synopsis   : int serializeHtmlLocation(Collection* coll, Location* self)
 * Input      : Collection* coll
 *              Location* self = what to latexalize
 * Output     : TRUE on success
 =======================================================================*/
static int 
serializeHtmlLocation(Collection* coll, Location* self)
{
  int rc = FALSE;
  Configuration* conf = 0;
  Server* server = 0;
  AVLNode *node = 0;
  FILE *fd = stdout;
  char *path = 0;
  char url[512];
  char text[640];
  int isHeader = FALSE;
  char mainHome[] = "../..";
  char headerHome[] = "../../../..";
  char* home = mainHome;
  int nb = 0, n = 0, i = 0;

  checkLocation(self);
  if (!(conf = getConfiguration())) goto error;

  // have to manage 2 cases :
  // - when use as main page
  // - when included from content list as header

  // if we need lists for the contents or not
  isHeader = ((nb = avl_count(self->childs))
  	      > MAX_INDEX_PER_PAGE);

  if (isHeader) {
    home = headerHome;

    // make dir locations/0000/000
    getLocationUri2(url, "/", self->id);
    if (!(path = createString(coll->htmlScoreDir))) goto error;
    if (!(path = catString(path, url))) goto error;
    if (mkdir(path, 0755)) {
      if (errno != EEXIST) {
    	logMain(LOG_ERR, "mkdir fails: %s", strerror(errno));
    	goto error;
      }
    }
  }
  
  // serialize the location shtml file. May be
  // - locations/0000/000.shtml or
  // - locations/0000/000/header.shtml
  if (!isHeader) {
    getLocationUri1(url, "/", self->id);
  } else {
    getLocationUri2(url, "/", self->id);
    sprintf(url + strlen(url), "%s", "/header.shtml");
    path = destroyString(path);
  }
  if (!(path = createString(coll->htmlScoreDir))) goto error;
  if (!(path = catString(path, url))) goto error;
  logMain(LOG_DEBUG, "serialize %s", path);
  if (!env.dryRun && (fd = fopen(path, "w")) == 0) {
    logMain(LOG_ERR, "fopen %s fails: %s", path, strerror(errno));
    goto error;
  }
  
  if (isHeader) {
    htmlSSIHeader2(fd, "../../../..", "score");
  } else {
    htmlSSIHeader(fd, "../../..", "score");
  }

  if ((strcmp(self->fingerPrint, "ALL"))) {
    if (!(server = getServer(coll, self->fingerPrint))) {
      logMain(LOG_ERR, "fails to get server %s", self->fingerPrint);
      goto error;
    }
    if (!sprintf(text, "%s %s:%s",  strLType(self->type),
		 server->host, self->uri)) goto error;
  }
  else {
    if (!sprintf(text, "%s ALL:%s", strLType(self->type), self->uri))
      goto error;
  }

  htmlPOpen(fd);
  htmlBold(fd, _("Location: "));
  htmlVerb(fd, text);
  htmlBr(fd);
 
  // servers
  htmlPOpen(fd);
  if ((strcmp(self->fingerPrint, "ALL"))) {
    if (!fprintf(fd, _("\nProvided by server:\n"))) goto error;
    htmlUlOpen(fd);
    if (!sprintf(text, "%s/servers/srv_%s.shtml",
		 home, server->fingerPrint)) goto error;
    htmlLiOpen(fd);
    htmlLink(fd, 0, text, server->host);
    htmlLiClose(fd);
    htmlUlClose(fd);
  }
  else {
    if (!fprintf(fd, _("\nProvided by all servers:\n"))) goto error;
  }
  htmlPClose(fd);
 
  // to
  htmlPOpen(fd);
  if (!fprintf(fd, _("\nContent: %i\n"), nb)) goto error;
  htmlUlOpen(fd);

  if (isHeader) {
    // - when included from content list as header
    path = destroyString(path);
    getLocationUri2(url, "/", self->id);
    if (!(path = createString(coll->htmlScoreDir))) goto error;
    if (!(path = catString(path, url))) goto error;

    // make content list directories: location/0000/000/0000
    n = (nb - 1) / MAX_INDEX_PER_PAGE +1;
    if (!htmlMakeDirs(path, n)) goto error;
    
    // serialize content lists
    node = self->childs->head;
    for (i=1 ; i<=n; ++i) {
      if (!serializeHtmlLocationContentList(coll, &node, self->id, i, n))
  	goto error;
    }
  }
  else {
    // - when use as main page
    if (avl_count(self->childs)) {
      htmlLocation(self, fd);
    } else {
      htmlLiOpen(fd);
      if (!fprintf(fd, _("No content\n"))) goto error;
      htmlLiClose(fd);
    }
  }

  htmlUlClose(fd);
  htmlPClose(fd);

  if (!isHeader) {
    htmlSSIFooter(fd, "../../..");
  }
  
  ++env.progBar.cur;
  rc = TRUE;
  if (!env.dryRun) {
    fclose(fd);
  } else {
    fflush(stdout);
  }
 error:
  if (!rc) {
    logMain(LOG_ERR, "serializeHtmlLocation fails");
  }
  path = destroyString(path);
  return rc;
}

/*=======================================================================
 * Function   : serializeHtmlServer
 * Description: HTML page for Server
 * Synopsis   : int serializeHtmlServer(Collection* coll, Server* server)
 * Input      : Collection* coll = context
 *              Server* server = server to serialize
 * Output     : TRUE on success
 =======================================================================*/
int 
serializeHtmlServer(Collection* coll, Server* server)
{ 
  int rc = FALSE;
  FILE* fd = stdout;
  char* path = 0;
  char* string = 0;
  Image* image = 0;
  Location* location = 0;
  RGIT* curr = 0;
  char url[128];
  char text[640];
  char score[8];
  int nbLoc = 0;
  struct tm date;

  checkServer(server);
  if (localtime_r(&server->lastCommit, &date) == (struct tm*)0) {
    logMain(LOG_ERR, "localtime_r returns on error");
    goto error;
  }
  
  if (!(path = createString(coll->htmlScoreDir))) goto error;
  if (!(path = catString(path, "/servers/srv_"))) goto error;
  if (!(path = catString(path, server->fingerPrint))) goto error;
  if (!(path = catString(path, ".shtml"))) goto error;
  logMain(LOG_DEBUG, "serialize %s", path);
  if (!env.dryRun && (fd = fopen(path, "w")) == 0) {
    logMain(LOG_ERR, "fopen %s fails: %s", path, strerror(errno)); 
    goto error;
  }  

  if (!sprintf(text, _("Server%s "),  
	       (server == coll->serverTree->master)?_(" master"):""))
    goto error;

  htmlSSIHeader(fd, "../..", "score");

  htmlPOpen(fd);
  htmlBold(fd, text);
  if (!htmlCaps(fd, server->host)) goto error;
  if (!fprintf(fd, " (%.2f)\n", server->score)) goto error;
  htmlBr(fd);
  if (!isEmptyString(server->comment)) {
    htmlItalic(fd, server->comment);  
    htmlBr(fd);
  }
  htmlPClose(fd);

  htmlPOpen(fd);
  htmlUlOpen(fd);
  htmlLiOpen(fd);
  if (!fprintf(fd, _("fingerprint: "))) goto error;
  htmlVerb(fd, server->fingerPrint);
  htmlLiClose(fd);
  htmlLiOpen(fd);
  if (!fprintf(fd, _("last commit: "))) goto error;
  if (!sprintf(text, "%04i-%02i-%02i,%02i:%02i:%02i",
	       date.tm_year + 1900, date.tm_mon+1, date.tm_mday,
	       date.tm_hour, date.tm_min, date.tm_sec)) goto error;
  htmlVerb(fd, text);
  htmlLiClose(fd);
  htmlLiOpen(fd);
  if (!fprintf(fd, _("mdtx port: "))) goto error;
  if (!sprintf(text, "%i", server->mdtxPort)) goto error;
  htmlVerb(fd, text);
  htmlLiClose(fd);
  htmlLiOpen(fd);
  if (!sprintf(text, "%i", server->sshPort)) goto error;
  if (!fprintf(fd, _("ssh port: "))) goto error;
  htmlVerb(fd, text);
  htmlLiClose(fd);
  htmlUlClose(fd);
  htmlPClose(fd);

  // networks
  if (!isEmptyRing(server->networks)) {
    if (!rgSort(server->networks, cmpString)) goto error;
    htmlPOpen(fd);
    if (!fprintf(fd, _("On networks:\n"))) goto error;
    htmlUlOpen(fd);
				    
    curr = 0;
    while ((string = rgNext_r(server->networks, &curr))) {    
      htmlLiOpen(fd);
      if (!fprintf(fd, "%s\n", string)) goto error;
      htmlLiClose(fd);
    }
    htmlUlClose(fd);
    htmlPClose(fd);
  }

  // gateways
  if (!isEmptyRing(server->gateways)) {
    if (!rgSort(server->gateways, cmpString)) goto error;
    htmlPOpen(fd);
    if (!fprintf(fd, _("Gateway for:\n"))) goto error;
    htmlUlOpen(fd);
				    
    curr = 0;
    while ((string = rgNext_r(server->gateways, &curr))) {    
      htmlLiOpen(fd);
      if (!fprintf(fd, "%s\n", string)) goto error;
      htmlLiClose(fd);
    }
    htmlUlClose(fd);
    htmlPClose(fd);
  }

  // images
  if (!isEmptyRing(server->images)) {
    if (!rgSort(server->images, cmpImage)) goto error;
    htmlPOpen(fd);
    if (!fprintf(fd, _("Provide images: %i\n"), server->images->nbItems)) 
      goto error;
    htmlUlOpen(fd);
				    
    curr = 0;
    while ((image = rgNext_r(server->images, &curr))) {    
      getArchiveUri(url, "..", image->archive);
      if (!sprintf(text, "%s:%lli ", image->archive->hash, 
		   (long long int)image->archive->size)) goto error;
      if (!sprintf(score, "(%.2f)", image->score)) goto error;

      htmlLiOpen(fd);
      htmlVerb(fd, text);
      htmlLink(fd, 0, url, score);
      htmlLiClose(fd);
    }
    htmlUlClose(fd);
    htmlPClose(fd);
  }

  // locations
  while ((location = rgNext_r(coll->extractTree->locations, &curr))) {    
    if (!(strcmp(location->fingerPrint, server->fingerPrint))) ++nbLoc;
  }
  if (nbLoc) {
    htmlPOpen(fd);
    if (!fprintf(fd, _("Provide locations: %i\n"), nbLoc)) 
      goto error;
    htmlUlOpen(fd);
				    
    curr = 0;
    while ((location = rgNext_r(coll->extractTree->locations, &curr))) {
      if (strcmp(location->fingerPrint, server->fingerPrint)) continue;
      getLocationUri(url, "..", location);
      if (!sprintf(text, "%s %s:%s", strLType(location->type),
		 server->host, location->uri))
      goto error;
  
      htmlLiOpen(fd);
      htmlLink(fd, 0, url, text);
      htmlLiClose(fd);
    }
    htmlUlClose(fd);
    htmlPClose(fd);
  }
  
  htmlSSIFooter(fd, "../..");

  rc = TRUE;
  if (!env.dryRun) {
    fclose(fd);
  } else {
    fflush(stdout);
  }
 error:
  if (!rc) {
    logMain(LOG_ERR, "serializeHtmlServer fails");
  }
  path = destroyString(path);
  return rc;
}


/*=======================================================================
 * Function   : serializeHtmlArchiveList
 * Description: Latexalize an Archive list.
 * Synopsis   : int serializeHtmlArchiveList(Collection* coll, 
 *                                                       int i, int n)
 * Input      : Collection* coll
 *              int i = current archive list
 *              int n = last archive list
 * Output     : TRUE on success
 =======================================================================*/
static int 
serializeHtmlArchiveList(Collection* coll, AVLNode **node, int i, int n)
{
  int rc = FALSE;
  Archive* archive = 0;
  FILE *fd = stdout;
  char *path = 0;
  char url[128];
  char text[128];
  int j = 0;

  getArchiveListUri(url, "/", i);
  if (!(path = createString(coll->htmlScoreDir))) goto error;
  if (!(path = catString(path, url))) goto error;
  logMain(LOG_DEBUG, "serialize %s", path);
  if (!env.dryRun && (fd = fopen(path, "w")) == 0) {
    logMain(LOG_ERR, "fopen %s fails: %s", path, strerror(errno)); 
    goto error;
  }  

  fprintf(fd, "<!--#include virtual='../header.shtml' -->");
  if (!node) goto end; // empty page if needed

  htmlPOpen(fd);
  if (!serializeHtmlListBar(coll, fd, i, n)) goto error;

  htmlUlOpen(fd);
  for (j = 0; j < MAX_INDEX_PER_PAGE && *node; ++j) {
    archive = (Archive*)(*node)->item;

    getArchiveUri(url, "../..", archive);
    if (!sprintf(text, "%s:%lli ", archive->hash, 
		 (long long int)archive->size)) goto error;

    htmlLiOpen(fd);
    htmlVerb(fd, text);
    htmlLink(fd, 0, url, getArchiveScore(archive));
    htmlLiClose(fd);

    *node = (*node)->next;
  }
  htmlUlClose(fd);

  if (!serializeHtmlListBar(coll, fd, i, n)) goto error;
  htmlPClose(fd);
 end:
  htmlSSIFooter(fd, "../../..");

  ++env.progBar.cur;
  rc = TRUE;
  if (!env.dryRun) {
    fclose(fd);
  } else {
    fflush(stdout);
  }
 error:
  if (!rc) {
    logMain(LOG_ERR, "serializeHtmlArchiveList fails");
  }
  path = destroyString(path);
  return rc;
}


/*=======================================================================
 * Function   : serializeHtmlArchiveLists
 * Description: HTML index page
 * Synopsis   : int 
 * Input      : ExtractTree* self = context
 * Output     : TRUE on success
 =======================================================================*/
static int 
serializeHtmlArchiveLists(Collection* coll)
{ 
  int rc = FALSE;
  AVLNode *node = 0;
  FILE* fd = stdout;
  char tmp[128];
  char* path = 0;
  char* path2 = 0;
  int nb = 0;
  int i = 0;
  int n = 0;

  logMain(LOG_DEBUG, "serializeHtmsScoreLists");

  nb = coll->maxId;
  if (!getArchiveListUri(tmp, "/", 0)) goto error;
  if (!(path = createString(coll->htmlScoreDir))) goto error;
  if (!(path = catString(path, tmp))) goto error;

  // build the directory
  if (mkdir(path, 0755)) {
    if (errno != EEXIST) {
      logMain(LOG_ERR, "mkdir fails: %s", strerror(errno));
      goto error;
    }
  }
  // serialize header file for the document list
  if (!(path2 = createString(path))) goto error;
  if (!(path2 = catString(path2, "/header.shtml"))) goto error;
  logMain(LOG_DEBUG, "Serialize %s", path2); 
  if (!env.dryRun && (fd = fopen(path2, "w")) == 0) {
    logMain(LOG_ERR, "fopen %s fails: %s", path2, strerror(errno)); 
    goto error;
  }  

  htmlSSIHeader2(fd, "../..", "score");
  htmlPOpen(fd);
  htmlBold(fd, _("All archives"));
  // nb may be greater
  if (!fprintf(fd, " : %i", avl_count(coll->archives))) goto error;
  htmlBr(fd);
  htmlPClose(fd);

  if (!env.dryRun) {
    fclose(fd);
  } else {
    fflush(stdout);
  }

  // get the total number of lists of archives
  n = (nb - 1) / MAX_INDEX_PER_PAGE +1;
  logMain(LOG_DEBUG, "have %i archive, so %i lists", nb, n);

  // build lists sub-directories (group by MAX_FILES_PER_DIR)
  if (!htmlMakeDirs(path, n)) goto error;
  
  // serialize lists
  node = coll->archives->head;
  for (i=1 ; i<=n; ++i) {
    if (!serializeHtmlArchiveList(coll, &node, i, n)) goto error;
  }

  // empty list if needed
  if (n == 0) {
    if (!htmlMakeDirs(path, 1)) goto error;
    if (!serializeHtmlArchiveList(coll, 0, 1, 0)) goto error;
  }

  rc = TRUE;
 error:
  if (!rc) {
    logMain(LOG_ERR, "serializeHtmlScoreScoreLists fails");
  }
  path = destroyString(path);
  path2 = destroyString(path2);
  return rc;
}


/*=======================================================================
 * Function   : serializeHtmlBadList
 * Description: Latexalize an bad archive list.
 * Synopsis   : int serializeHtmlBadList(Collection* coll, int i, int n)
 * Input      : Collection* coll
 *              int i = current bad archive list
 *              int n = last bad archive list
 * Output     : TRUE on success
 =======================================================================*/
static int 
serializeHtmlBadList(Collection* coll, RG* ring, int i, int n)
{
  int rc = FALSE;
  Archive* archive = 0;
  FILE *fd = stdout;
  char *path = 0;
  char url[128];
  char text[128];
  int j = 0;

  checkCollection(coll);

  getBadListUri(url, "/", i);
  if (!(path = createString(coll->htmlScoreDir))) goto error;
  if (!(path = catString(path, url))) goto error;

  logMain(LOG_DEBUG, "serialize %s", path);
  if (!env.dryRun && (fd = fopen(path, "w")) == 0) {
    logMain(LOG_ERR, "fopen %s fails: %s", path, strerror(errno)); 
    goto error;
  }  

  fprintf(fd, "<!--#include virtual='../header.shtml' -->");
  if (!ring) goto end; // empty page if needed

  htmlPOpen(fd);
  if (!serializeHtmlListBar(coll, fd, i, n)) goto error;

  htmlUlOpen(fd);
  archive = rgCurrent(ring);
  for (j = 0; j < MAX_INDEX_PER_PAGE && archive; ++j) {

    getArchiveUri(url, "../..", archive);
    if (!sprintf(text, "%s:%lli ", archive->hash,
		 (long long int)archive->size)) goto error;

    htmlLiOpen(fd);
    htmlVerb(fd, text);
    htmlLink(fd, 0, url, getArchiveScore(archive));
    htmlLiClose(fd);

    archive = rgNext(ring);
  }
  htmlUlClose(fd);

  if (!serializeHtmlListBar(coll, fd, i, n)) goto error;
  htmlPClose(fd);
 end:
  htmlSSIFooter(fd, "../../..");

  ++env.progBar.cur;
  rc = TRUE;
  if (!env.dryRun) {
    fclose(fd);
  } else {
    fflush(stdout);
  }
 error:
  if (!rc) {
    logMain(LOG_ERR, "serializeHtmlBadList fails");
  }
  path = destroyString(path);
  return rc;
}


/*=======================================================================
 * Function   : serializeHtmlBadLists
 * Description: HTML index page
 * Synopsis   : int 
 * Input      : ExtractTree* self = context
 * Output     : TRUE on success
 =======================================================================*/
static int 
serializeHtmlBadLists(Collection* coll)
{ 
  int rc = FALSE;
  RG* badArchives = 0;
  FILE* fd = stdout;
  char tmp[128];
  char* text = 0;
  char* path = 0;
  char* path2 = 0;
  off_t badSize = 0;
  int nb = 0;
  int i = 0;
  int n = 0;
  char buf[30];

  logMain(LOG_DEBUG, "serializeHtmsBadLists");

  // put bad archives into a new ring  
  if (!(badArchives = createRing())) goto error;
  if (!(text = getExtractStatus(coll, &badSize, &badArchives))) goto error;
  nb = badArchives->nbItems;

  if (!getBadListUri(tmp, "/", 0)) goto error;
  if (!(path = createString(coll->htmlScoreDir))) goto error;
  if (!(path = catString(path, tmp))) goto error;

  // build the directory
  if (mkdir(path, 0755)) {
    if (errno != EEXIST) {
      logMain(LOG_ERR, "mkdir fails: %s", strerror(errno));
      goto error;
    }
  }
  // serialize header file for the document list
  if (!(path2 = createString(path))) goto error;
  if (!(path2 = catString(path2, "/header.shtml"))) goto error;
  logMain(LOG_DEBUG, "Serialize %s", path2); 
  if (!env.dryRun && (fd = fopen(path2, "w")) == 0) {
    logMain(LOG_ERR, "fopen %s fails: %s", path2, strerror(errno)); 
    goto error;
  }
  
  htmlSSIHeader2(fd, "../..", "score");
  htmlPOpen(fd);
  htmlBold(fd, _("Bad images"));
  if (!fprintf(fd, " : %i", nb)) goto error;
  htmlBr(fd);
  htmlItalic(fd, text);
  htmlBr(fd);
  htmlPClose(fd);
  
  if (badSize > 0) {
    sprintSize(buf, badSize);
    if (!fprintf(fd, "%s\n", buf)) goto error;
    if (!fprintf(fd, "%s", _("should be burned into supports:")))
      goto error;
  }

  if (!env.dryRun) {
    fclose(fd);
  } else {
    fflush(stdout);
  }

  // sort this new ring on scores
  rgSort(badArchives, cmpArchiveScore);

  // get the total number of lists of documents
  n = (nb - 1) / MAX_INDEX_PER_PAGE +1;
  logMain(LOG_DEBUG, "have %i bad archives, so %i lists", nb, n);

  // build lists sub-directories (group by MAX_FILES_PER_DIR)
  if (!htmlMakeDirs(path, n)) goto error;
  
  // serialize lists
  rgHead(badArchives); // set current archive to the first one
  for (i=1 ; i<=n; ++i) {
    if (!serializeHtmlBadList(coll, badArchives, i, n)) goto error;
  }

  // empty list if needed
  if (n == 0) {
    if (!htmlMakeDirs(path, 1)) goto error;
    if (!serializeHtmlBadList(coll, 0, 1, 0)) goto error;
  }

  rc = TRUE;
 error:
  if (!rc) {
    logMain(LOG_ERR, "serializeHtmlBadLists fails");
  }
  path = destroyString(path);
  path2 = destroyString(path2);
  text = destroyString(text);
  badArchives = destroyOnlyRing(badArchives);
  return rc;
}


/*=======================================================================
 * Function   : serializeHtmlImageList
 * Description: Latexalize an image list.
 * Synopsis   : int serializeHtmlImageList(Collection* coll, int i, int n)
 * Input      : Collection* coll
 *              int i = current image list
 *              int n = last image list
 * Output     : TRUE on success
 =======================================================================*/
static int 
serializeHtmlImageList(Collection* coll, RG* ring, int i, int n)
{
  int rc = FALSE;
  Archive* archive = 0;
  FILE *fd = stdout;
  char *path = 0;
  char url[128];
  char text[128];
  int j = 0;

  checkCollection(coll);

  getImageListUri(url, "/", i);
  if (!(path = createString(coll->htmlScoreDir))) goto error;
  if (!(path = catString(path, url))) goto error;

  logMain(LOG_DEBUG, "serialize %s", path);
  if (!env.dryRun && (fd = fopen(path, "w")) == 0) {
    logMain(LOG_ERR, "fopen %s fails: %s", path, strerror(errno)); 
    goto error;
  }  

  fprintf(fd, "<!--#include virtual='../header.shtml' -->");
  if (!ring) goto end; // empty page if needed

  htmlPOpen(fd);
  if (!serializeHtmlListBar(coll, fd, i, n)) goto error;

  htmlUlOpen(fd);
  archive = rgCurrent(ring);
  for (j = 0; j < MAX_INDEX_PER_PAGE && archive; ++j) {

    getArchiveUri(url, "../..", archive);
    if (!sprintf(text, "%s:%lli ", archive->hash,
		 (long long int)archive->size)) goto error;

    htmlLiOpen(fd);
    htmlVerb(fd, text);
    htmlLink(fd, 0, url, getArchiveScore(archive));
    htmlLiClose(fd);

    archive = rgNext(ring);
  }
  htmlUlClose(fd);

  if (!serializeHtmlListBar(coll, fd, i, n)) goto error;
  htmlPClose(fd);
 end:
  htmlSSIFooter(fd, "../../..");

  ++env.progBar.cur;
  rc = TRUE;
  if (!env.dryRun) {
    fclose(fd);
  } else {
    fflush(stdout);
  }
 error:
  if (!rc) {
    logMain(LOG_ERR, "serializeHtmlImageList fails");
  }
  path = destroyString(path);
  return rc;
}


/*=======================================================================
 * Function   : serializeHtmlImageLists
 * Description: HTML index page
 * Synopsis   : int 
 * Input      : ExtractTree* self = context
 * Output     : TRUE on success
 =======================================================================*/
static int 
serializeHtmlImageLists(Collection* coll)
{ 
  int rc = FALSE;
  ServerTree* serverTree = 0;
  FILE* fd = stdout;
  char tmp[128];
  char* path = 0;
  char* path2 = 0;
  int nb = 0;
  int i = 0;
  int n = 0;

  logMain(LOG_DEBUG, "serializeHtmsImagesLists");
  if (!(serverTree = coll->serverTree)) goto error;
  
  nb = serverTree->archives->nbItems;
  
  if (!getImageListUri(tmp, "/", 0)) goto error;
  if (!(path = createString(coll->htmlScoreDir))) goto error;
  if (!(path = catString(path, tmp))) goto error;

  // build the directory
  if (mkdir(path, 0755)) {
    if (errno != EEXIST) {
      logMain(LOG_ERR, "mkdir fails: %s", strerror(errno));
      goto error;
    }
  }
  // serialize header file for the image list
  if (!(path2 = createString(path))) goto error;
  if (!(path2 = catString(path2, "/header.shtml"))) goto error;
  logMain(LOG_DEBUG, "Serialize %s", path2); 
  if (!env.dryRun && (fd = fopen(path2, "w")) == 0) {
    logMain(LOG_ERR, "fopen %s fails: %s", path2, strerror(errno)); 
    goto error;
  }
  
  htmlSSIHeader2(fd, "../..", "score");
  htmlPOpen(fd);
  htmlBold(fd, _("Images"));
  if (!fprintf(fd, " : %i", nb)) goto error;
  htmlBr(fd);
  htmlPClose(fd);
  
  if (!env.dryRun) {
    fclose(fd);
  } else {
    fflush(stdout);
  }

  // sort ring
  if (!rgSort(serverTree->archives, cmpArchive)) goto error;

  // get the total number of lists of documents
  n = (nb - 1) / MAX_INDEX_PER_PAGE +1;
  logMain(LOG_DEBUG, "have %i images, so %i lists", nb, n);

  // build lists sub-directories (group by MAX_FILES_PER_DIR)
  if (!htmlMakeDirs(path, n)) goto error;
  
  // serialize lists
  rgHead(serverTree->archives); // set current image to the first one
  for (i=1 ; i<=n; ++i) {
    if (!serializeHtmlImageList(coll, serverTree->archives, i, n)) goto error;
  }

  // empty list if needed
  if (n == 0) {
    if (!htmlMakeDirs(path, 1)) goto error;
    if (!serializeHtmlImageList(coll, 0, 1, 0)) goto error;
  }

  rc = TRUE;
 error:
  if (!rc) {
    logMain(LOG_ERR, "serializeHtmlImageLists fails");
  }
  path = destroyString(path);
  path2 = destroyString(path2);
  return rc;
}


/*=======================================================================
 * Function   : serializeHtmlLocationList
 * Description: Latexalize an location list.
 * Synopsis   : int serializeHtmlLocationList(Collection* coll, int i, int n)
 * Input      : Collection* coll
 *              RG* ring of locations
 *              int i = current location list
 *              int n = last location list
 * Output     : TRUE on success
 =======================================================================*/
static int 
serializeHtmlLocationList(Collection* coll, RG* ring, int i, int n)
{
  int rc = FALSE;
  Location* location = 0;
  Server* server = 0;
  FILE *fd = stdout;
  char *path = 0;
  char url[128];
  char text[640];
  int j = 0;

  checkCollection(coll);

  getLocationListUri(url, "/", i);
  if (!(path = createString(coll->htmlScoreDir))) goto error;
  if (!(path = catString(path, url))) goto error;

  logMain(LOG_DEBUG, "serialize %s", path);
  if (!env.dryRun && (fd = fopen(path, "w")) == 0) {
    logMain(LOG_ERR, "fopen %s fails: %s", path, strerror(errno)); 
    goto error;
  }  

  fprintf(fd, "<!--#include virtual='../header.shtml' -->");
  if (!ring) goto end; // empty page if needed

  htmlPOpen(fd);
  if (!serializeHtmlListBar(coll, fd, i, n)) goto error;

  htmlUlOpen(fd);
  location = rgCurrent(ring);
  for (j = 0; j < MAX_INDEX_PER_PAGE && location; ++j) {
    
    getLocationUri(url, "../..", location);
    if ((strcmp(location->fingerPrint, "ALL"))) {
      if (!(server = getServer(coll, location->fingerPrint))) {
	logMain(LOG_ERR, "fails to get server %s", location->fingerPrint);
	goto error;
      }
      if (!sprintf(text, "%s %s:%s", strLType(location->type),
		   server->host, location->uri)) goto error;
    }
    else {
      if (!sprintf(text, "%s ALL:%s", strLType(location->type),
		   location->uri)) goto error;
    }

    htmlLiOpen(fd);
    htmlLink(fd, 0, url, text);
    htmlLiClose(fd);

    location = rgNext(ring);
  }
  htmlUlClose(fd);

  if (!serializeHtmlListBar(coll, fd, i, n)) goto error;
  htmlPClose(fd);
 end:
  htmlSSIFooter(fd, "../../..");

  ++env.progBar.cur;
  rc = TRUE;
  if (!env.dryRun) {
    fclose(fd);
  } else {
    fflush(stdout);
  }
 error:
  if (!rc) {
    logMain(LOG_ERR, "serializeHtmlLocationList fails");
  }
  path = destroyString(path);
  return rc;
}


/*=======================================================================
 * Function   : serializeHtmlLocationLists
 * Description: HTML index page
 * Synopsis   : int 
 * Input      : ExtractTree* self = context
 * Output     : TRUE on success
 =======================================================================*/
static int 
serializeHtmlLocationLists(Collection* coll)
{ 
  int rc = FALSE;
  ExtractTree* extractTree = 0;
  FILE* fd = stdout;
  char tmp[128];
  char* path = 0;
  char* path2 = 0;
  int nb = 0;
  int i = 0;
  int n = 0;

  logMain(LOG_DEBUG, "serializeHtmsLocationLists");
  if (!(extractTree = coll->extractTree)) goto error;
  
  nb = extractTree->locations->nbItems;
  
  if (!getLocationListUri(tmp, "/", 0)) goto error;
  if (!(path = createString(coll->htmlScoreDir))) goto error;
  if (!(path = catString(path, tmp))) goto error;

  // build the directory
  if (mkdir(path, 0755)) {
    if (errno != EEXIST) {
      logMain(LOG_ERR, "mkdir fails: %s", strerror(errno));
      goto error;
    }
  }
  // serialize header file for the location list
  if (!(path2 = createString(path))) goto error;
  if (!(path2 = catString(path2, "/header.shtml"))) goto error;
  logMain(LOG_DEBUG, "Serialize %s", path2); 
  if (!env.dryRun && (fd = fopen(path2, "w")) == 0) {
    logMain(LOG_ERR, "fopen %s fails: %s", path2, strerror(errno)); 
    goto error;
  }
  
  htmlSSIHeader2(fd, "../..", "score");
  htmlPOpen(fd);
  htmlBold(fd, _("Locations"));
  if (!fprintf(fd, " : %i", nb)) goto error;
  htmlBr(fd);
  htmlPClose(fd);
  
  if (!env.dryRun) {
    fclose(fd);
  } else {
    fflush(stdout);
  }

  // sort already done

  // get the total number of lists of documents
  n = (nb - 1) / MAX_INDEX_PER_PAGE +1;
  logMain(LOG_DEBUG, "have %i locations, so %i lists", nb, n);

  // build lists sub-directories (group by MAX_FILES_PER_DIR)
  if (!htmlMakeDirs(path, n)) goto error;
  
  // serialize lists
  rgHead(extractTree->locations); // set current location to the first one
  for (i=1 ; i<=n; ++i) {
    if (!serializeHtmlLocationList(coll, extractTree->locations, i, n)) goto error;
  }

  // empty list if needed
  if (n == 0) {
    if (!htmlMakeDirs(path, 1)) goto error;
    if (!serializeHtmlLocationList(coll, 0, 1, 0)) goto error;
  }

  rc = TRUE;
 error:
  if (!rc) {
    logMain(LOG_ERR, "serializeHtmlLocationLists fails");
  }
  path = destroyString(path);
  path2 = destroyString(path2);
  return rc;
}


/*=======================================================================
 * Function   : serializeHtmsScoreIndex
 * Description: HTML index page
 * Synopsis   : int serializeHtmsScoreIndex(Collection* coll)
 * Input      : ExtractTree* self = what to latexalize
 * Output     : TRUE on success
 =======================================================================*/
static int 
serializeHtmsScoreIndex(Collection* coll)
{ 
  int rc = FALSE;
  FILE* fd = stdout;
  char* path = 0;
  ServerTree* serverTree = 0;
  //Archive* archive = 0;
  //char url[128];
  //char text[128];

  if (!(serverTree = coll->serverTree)) goto error;

  if (!(path = createString(coll->htmlScoreDir))) goto error;
  if (!(path = catString(path, "/index.shtml"))) goto error;
  logMain(LOG_DEBUG, "serialize %s", path);
  if (!env.dryRun && (fd = fopen(path, "w")) == 0) {
    logMain(LOG_ERR, "fdopen %s fails: %s", path, strerror(errno)); 
    goto error;
  }  

  htmlSSIHeader(fd, "..", _("score"));

  // score parameter of the collection
  htmlPOpen(fd);
  if (!fprintf(fd, _("\nScore parameters:\n"))) goto error;
  htmlUlOpen(fd);
  htmlLiOpen(fd);
  printLapsTime(fd, "%-10s:", "serverTTL",  serverTree->serverTTL);
  htmlLiClose(fd);
  htmlLiOpen(fd);
  printLapsTime(fd, "%-10s:", "uploadTTL",  serverTree->uploadTTL);
  htmlLiClose(fd);
  htmlLiOpen(fd);
  printLapsTime(fd, "%-10s:", "suppTTL",  serverTree->scoreParam.suppTTL);
  htmlLiClose(fd);
  htmlLiOpen(fd);
  fprintf(fd, "%-10s: %.2f\n", "maxScore", serverTree->scoreParam.maxScore);
  htmlLiClose(fd);
  htmlLiOpen(fd);
  fprintf(fd, "%-10s: %.2f\n", "badScore", serverTree->scoreParam.badScore);
  htmlLiClose(fd);
  htmlLiOpen(fd);
  fprintf(fd, "%-10s: %.2f\n", "powSupp",  serverTree->scoreParam.powSupp);
  htmlLiClose(fd);
  htmlLiOpen(fd);
  fprintf(fd, "%-10s: %.2f\n", "factSupp", serverTree->scoreParam.factSupp);
  htmlLiClose(fd);
  htmlLiOpen(fd);
  fprintf(fd, "%-10s: %.2f\n", "fileScore", serverTree->scoreParam.fileScore);
  htmlLiClose(fd);
  htmlLiOpen(fd);
  fprintf(fd,  "%-10s: %i", "minGeoDup", serverTree->minGeoDup);
  htmlLiClose(fd);
  htmlUlClose(fd);
  htmlPClose(fd);

  // score parameter of the collection
  htmlPOpen(fd);
  if (!fprintf(fd, _("\nSelf-ingestion parameters:\n"))) goto error;
  htmlUlOpen(fd);
  htmlLiOpen(fd);
  fprintf(fd, "%-10s: %s", "logApache",
	  serverTree->log & APACHE?"yes":"no");
  htmlLiClose(fd);
  htmlLiOpen(fd);
  fprintf(fd, "%-10s: %s", "logCvs", serverTree->log & GIT?"yes":"no");
  htmlLiClose(fd);
  htmlLiOpen(fd);
  fprintf(fd, "%-10s: %s", "logAudit", serverTree->log & AUDIT?"yes":"no");
  htmlLiClose(fd);
  htmlUlClose(fd);
  htmlPClose(fd);

  /* // images */
  /* htmlPOpen(fd); */
  /* if (!fprintf(fd, _("\nAll images: %i\n"),  */
  /* 	       serverTree->archives->nbItems)) goto error; */
  /* htmlUlOpen(fd); */
  /* if (!isEmptyRing(serverTree->archives)) { */
  /*   if (!rgSort(serverTree->archives, cmpArchive)) goto error; */
  /*   while ((archive = rgNext(serverTree->archives))) { */
  /*     getArchiveUri(url, "", archive); */
  /*     if (!sprintf(text, "%s:%lli ", archive->hash,  */
  /* 		   (long long int)archive->size)) goto error; */

  /*     htmlLiOpen(fd); */
  /*     htmlVerb(fd, text); */
  /*     htmlLink(fd, 0, url, getArchiveScore(archive)); */

  /*     htmlLiClose(fd); */
  /*   } */
  /* } */
  /* else { */
  /*   htmlLiOpen(fd); */
  /*   if (!fprintf(fd, _("No image\n"))) goto error; */
  /*   htmlLiClose(fd); */
  /* } */
  /* htmlUlClose(fd); */
  /* htmlPClose(fd); */

  htmlSSIFooter(fd, "..");

  if (!env.dryRun) {
    fclose(fd);
  } else {
    fflush(stdout);
  }
  rc = TRUE;
 error:
  if (!rc) {
    logMain(LOG_ERR, "serializeHtmsScoreIndex fails");
  }
  path = destroyString(path);
  return rc;
}


/*=======================================================================
 * Function   : serializeHtmlScoreHeader
 * Description: This template will be used by apache SSI
 * Synopsis   : static int serializeHtmlScoreHeader(Collection* coll)
 * Input      : Collection* coll: input collection
 * Output     : TRUE on success
 =======================================================================*/
static int 
serializeHtmlScoreHeader(Collection* coll)
{ 
  int rc = FALSE;
  ServerTree* self = 0;
  Server* server = 0;
  FILE* fd = stdout;
  char* path = 0;
  char url[512];

  if (!(coll->extractTree)) goto error;
  if (!(self = coll->serverTree)) goto error;

  if (!(path = createString(coll->htmlDir))) goto error;
  if (!(path = catString(path, "/scoreHeader.shtml"))) goto error;
  logMain(LOG_DEBUG, "serialize %s", path);
  if (!env.dryRun && (fd = fopen(path, "w")) == 0) {
    logMain(LOG_ERR, "fdopen %s fails: %s", path, strerror(errno)); 
    goto error;
  }  

  if (!htmlMainHead(fd, _("Score"))) goto error;
  if (!htmlLeftPageHead(fd, _("score"), 0, self->dnsUrl)) goto error;

  // global score
  if (!fprintf(fd, "(%.2f)\n", coll->extractTree->score)) goto error;
  htmlBr(fd);

  // archive lists
  getArchiveListUri(url, "<!--#echo var='HOME' -->/score", 1);
  htmlLink(fd, 0, url, _("All archives"));
  htmlBr(fd);

  // archive with bad score lists
  getBadListUri(url, "<!--#echo var='HOME' -->/score", 1);
  htmlLink(fd, 0, url, _("Bad images"));
  htmlBr(fd);

  // images lists
  getImageListUri(url, "<!--#echo var='HOME' -->/score", 1);
  htmlLink(fd, 0, url, _("Images"));
  htmlBr(fd);

  // locations lists
  getLocationListUri(url, "<!--#echo var='HOME' -->/score", 1);
  htmlLink(fd, 0, url, _("Locations"));
  htmlBr(fd);
  
  // each server
  if (!isEmptyRing(self->servers)) {
    // sort already done
    htmlUlOpen(fd);

    rgRewind(self->servers);
    while ((server = rgNext(self->servers))) {
      if (!sprintf(url, 
		   "<!--#echo var='HOME' -->/score/servers/srv_%s.shtml", 
		   server->fingerPrint)) goto error;
    
      htmlLiOpen(fd);
      htmlLink(fd, 0, url, server->host);
      htmlLiClose(fd);
    }
    htmlUlClose(fd);
  }

  if (!htmlLeftPageTail(fd)) goto error;
  if (!htmlRightHead(fd)) goto error;

  if (!env.dryRun) {
    fclose(fd);
  } else {
    fflush(stdout);
  }
  rc = TRUE;
 error:
  if (!rc) {
    logMain(LOG_ERR, "serializeHtmlScoreHeader fails");
  }
  path = destroyString(path);
  return rc;
}


/*=======================================================================
 * Function   : serializeHtmlScore 
 * Description: Generate HTML for score directory
 * Synopsis   : int serializeHtmlScore(Collection* coll)
 * Input      : Collection* coll: input collection
 * Output     : TRUE on success
 =======================================================================*/
int 
serializeHtmlScore(Collection* coll)
{ 
  int rc = FALSE;
  ServerTree* serverTree = 0;
  ExtractTree* extractTree = 0;
  Server* server = 0;
  Archive *archive = 0;
  Location *location = 0;
  AVLNode *node = 0;
  char tmp[128];
  char *path1 = 0;
  char *path2 = 0;
  int nb = 0;

  checkCollection(coll);
  if (!(serverTree = coll->serverTree)) goto error;
  if (!(extractTree = coll->extractTree)) goto error;
  logMain(LOG_DEBUG, "serializeHtmlScore: %s collection", coll->label);
 
  // server's directories
  if (!(path1 = createString(coll->htmlScoreDir))) goto error;
  if (!(path1 = catString(path1, "/servers"))) goto error;
  if (mkdir(path1, 0755)) {
    if (errno != EEXIST) {
      logMain(LOG_ERR, "mkdir fails: %s", strerror(errno));
      goto error;
    }
  }

  // servers
  if (!isEmptyRing(serverTree->servers)) {
    if (!rgSort(serverTree->servers, cmpServer)) goto error;
    while ((server = rgNext(serverTree->servers))) {
      if (!serializeHtmlServer(coll, server)) goto error;
    }
  }

  // archive's directories
  path1 = destroyString(path1);
  if (!getArchiveUri1(tmp, "/", -1)) goto error;
  if (!(path1 = createString(coll->htmlScoreDir))) goto error;
  if (!(path1 = catString(path1, tmp))) goto error;
  if (mkdir(path1, 0755)) {
    if (errno != EEXIST) {
      logMain(LOG_ERR, "mkdir %s fails: %s", path1, strerror(errno));
      goto error;
    }
  }

  // archives
  if ((nb = coll->maxId)) {
    logMain(LOG_DEBUG, "have %i archives", nb);
    if (!htmlMakeDirs(path1, nb)) goto error;
    for(node = coll->archives->head; node; node = node->next) {
      archive = (Archive*)node->item;
      if (!serializeHtmlScoreArchive(coll, archive)) goto error;
    }
  }

  // loacation's directories
  path1 = destroyString(path1);
  if (!getLocationUri1(tmp, "/", -1)) goto error;
  if (!(path1 = createString(coll->htmlScoreDir))) goto error;
  if (!(path1 = catString(path1, tmp))) goto error;
  if (mkdir(path1, 0755)) {
    if (errno != EEXIST) {
      logMain(LOG_ERR, "mkdir %s fails: %s", path1, strerror(errno));
      goto error;
    }
  }

  // locations
  if ((nb = extractTree->maxIdLocation)) {
    logMain(LOG_DEBUG, "have %i locations", nb);
    if (!htmlMakeDirs(path1, nb)) goto error;
    if (!rgSort(extractTree->locations, cmpServer)) goto error;
    while ((location = rgNext(extractTree->locations))) {
      if (!serializeHtmlLocation(coll, location)) goto error;
    }
  }
  
  // others
  if (!serializeHtmlArchiveLists(coll)) goto error;
  if (!serializeHtmlBadLists(coll)) goto error;
  if (!serializeHtmlImageLists(coll)) goto error;
  if (!serializeHtmlLocationLists(coll)) goto error;
  if (!serializeHtmsScoreIndex(coll)) goto error;
  if (!serializeHtmlScoreHeader(coll)) goto error;
  
  rc = TRUE;
 error:
  if (!rc) {
    logMain(LOG_ERR, "serializeHtmlScore fails");
  }
  path1 = destroyString(path1);
  path2 = destroyString(path2);
  return rc;
}

/* Local Variables: */
/* mode: c */
/* mode: font-lock */
/* mode: auto-fill */
/* End: */

