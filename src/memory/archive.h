/*=======================================================================
 * Project: MediaTeX
 * Module : archive tree
 *
 * Archive producer interface

 MediaTex is an Electronic Archives Management System
 Copyright (C) 2014 2015 2016 2017 2018 Nicolas Roche
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
=======================================================================*/

#ifndef MDTX_MEMORY_ARCHIVE_H
#define MDTX_MEMORY_ARCHIVE_H 1

#include "mediatex-types.h"

// type written into Record struct
typedef enum {UNUSED = 0, USED, WANTED, ALLOCATED, AVAILABLE, TOKEEP,
	      ASTATE_MAX} AState;

// type written into Archive.manage:
//  locations are facilities to serve files (cgi+server) that are not 
//  managed as archives by mediatex as true archives.
typedef enum {NO_RULE = 0, LOCATION_RULES, EXTRACT_RULES} AManage;

struct Archive
{
  // not easy to parse into uchar[16]
  char  hash[MAX_SIZE_MD5+1]; 

  // off_t must be 64bit to manage CDRom size
  // (_FILE_OFFSET_BITS set to 64 by mediatex.h)
  off_t size;
  
  // serverTree related data
  RG*   images;
  float imageScore;

  // extractTree related data
  RG*        fromContainers; // (FromAsso*)
  Container* toContainer;    // only one: (choose a rule TGZ or TAR+GZ)
  time_t     uploadTime;     // uploaded archive (from INC container)
  char*      imgExtractionPath; // image extraction path (from IMG container)
  int        incInherency;   // is a new incoming (even by inherency)
  float      extractScore;   // computed value used by cache
  RG*        fromLocations;  // (FromLocation*)

  // documentTree related data
  RG* documents;
  RG* assoCaracs;

  // recordTree related data
  RG* records; // related record to destroy if we remove this archive

  // cacheTree related to data
  AState  state;
  RG*     demands;        // Record*
  RG*     remoteSupplies; // Record*
  RG*     finalSupplies;  // Record*
  Record* localSupply;
  int     nbKeep;
  time_t  backupDate;     // date to set after last unkeep

  // misc
  int   id;      // id used to serialize html pages
};

/* API */
char* strAState(AState state);
int cmpArchive(const void *p1, const void *p2);
int cmpArchiveAvl(const void *p1, const void *p2);
int cmpArchiveScore(const void *p1, const void *p2);
Archive* getArchive(Collection* coll, char* hash, off_t size);
Archive* addArchive(Collection* coll, char* hash, off_t size);

int isIncoming(Collection* coll, Archive* self);
int isBadTopContainer(Collection* coll, Archive* archive);
int belongsToContainers(Collection* coll, Archive* archive);
int belongsToExtractTree(Collection* coll, Archive* archive);

/* Private */
Archive* createArchive(void);
Archive* destroyArchive(Archive* self);
int diseaseArchive(Collection* coll, Archive* arch);
int diseaseArchives(Collection* coll);

#endif /* MDTX_MEMORY_ARCHIVE_H */

/* Local Variables: */
/* mode: c */
/* mode: font-lock */
/* End: */
