/*=======================================================================
 * Project: MediaTeX
 * Module : extraction tree
 *
 * Extraction producer interface

 MediaTex is an Electronic Records Management System
 Copyright (C) 2014 2015 2016 2017 2018 Nicolas Roche
 
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <http://www.gnu.org/licenses/>.
=======================================================================*/

#ifndef MDTX_MEMORY_EXTRACT_H
#define MDTX_MEMORY_EXTRACT_H 1

#include "mediatex-types.h"

// Container types
typedef enum {INC=1,           // for uploads not already burned
	      IMG,             // tips to remind supports extraction path
	      ISO,             // CD image
	      CAT,             // generic way to do multi-volume
	      TGZ, TBZ, AFIO,  // GNU/Linux agregation + compression
	      TAR, CPIO,       // GNU/Linux agregation only
	      GZIP, BZIP,      // GNU/Linux compression only  
	      ZIP, RAR,        // windows agregation + compression
	      ETYPE_MAX} EType;

// note: native multi-volume is only managed for rar archives
//       (this should be done later for the others too)
//       however, split/cat provide a generic way to do that for all

// Location types
typedef enum {COPY=1,         // external files on local directory
	      // external files on remote web server:
	      GET,            // wget URL + postfix
	      POST,           // wget URL + post-data
	      LTYPE_MAX} LType;

// Record from container association
struct FromAsso {
  Archive*   archive;    // id
  Container* container;  // id
  char*      path;            
};

struct Container {
  // the 2 entries bellow are the primary key
  EType    type;         // container type: IMG, CAT, TGZ, ISO...
  Archive* parent;       // same as the first entry in bellow ring
    
  RG*      parents;      // ex: RAR source files (Archives*)
  char*    directory;    // prefix to add to child's extraction paths
  AVLTree* childs;       // ex: RAR target files (FromAsso*)

  float    score;        // use by mdtx-make
  int      incInherency; // is a new incoming (even by inherency)
};

// Record from location association
struct LocationAsso {
  Archive*   archive;    // id
  Location*  location;   // id
  char*      path;            
};

// Read-only data directory on the hard drive of a server (special container)
struct Location {
  // the 4 entries bellow are the primary key
  LType type;                        // location type: FILE, URL      
  char  fingerPrint[MAX_SIZE_MD5+1]; // server id
  char* uri;                         // read-only absolute data directory path,
                                     //  or url
  char*    directory;                // prefix to add to child's extraction paths
  char*    options;                  // more options for wget
  AVLTree* childs;                   // (LocationAsso*)

  // misc
  int   id;                          // id used to serialize html pages
};

struct ExtractTree {
  AVLTree* containers;

  // special container with no parents
  Container* inc;        // provides unsafe incomings
  Container* img;        // provides top image extraction's path
  RG*        locations;  // (Location*)

  float score;           // global score for the collection
  int maxIdLocation;     // id used to serialize archives on html pages
};

char* strEType(EType self);
EType getEType(char* label);

FromAsso* createFromAsso(void);
FromAsso* destroyFromAsso(FromAsso* self);
int cmpFromAssoAvl(const void *p1, const void *p2);

Container* createContainer(void);
Container* destroyContainer(Container* self);
//int cmpContainer(const void *p1, const void *p2);
int cmpContainerAvl(const void *p1, const void *p2);
int serializeContainer(Collection* coll, Container* self, CvsFile* fd);

char* strLType(LType self);
LType getLType(char* label);

LocationAsso* createLocationAsso(void);
LocationAsso* destroyLocationAsso(LocationAsso* self);
int cmpLocationAssoAvl(const void *p1, const void *p2);

Location* createLocation(void);
Location* destroyLocation(Location* self);
int cmpLocation(const void *p1, const void *p2);
int serializeLocation(Location* self, CvsFile* fd);

int serializeExtractRecord(Archive* self, CvsFile* fd);

ExtractTree* createExtractTree(void);
ExtractTree* destroyExtractTree(ExtractTree* self);
int serializeExtractTree(Collection* coll, CvsFile* fd);

/* API */

int addFromArchive(Collection* coll, Container* container, 
		   Archive* archive);
int delFromArchive(Collection* coll, Container* container, 
		   Archive* archive);

FromAsso* addFromAsso(Container* container, Archive* archive, char* path);
int delFromAsso(FromAsso* self);

Container* addContainer(Collection* coll, EType type, Archive* parent);
int delContainer(Collection* coll, Container* self);

LocationAsso* addLocationAsso(Location* location, Archive* archive, char* uri);
int delLocationAsso(LocationAsso* self);

Location* addLocation(Collection* coll, LType type, char* fingerPrint, char* path, char* dir);
int delLocation(Collection* coll, Location* self);

int diseaseExtractTree(Collection* coll);

#endif /* MDTX_MEMORY_EXTRACT_H */

/* Local Variables: */
/* mode: c */
/* mode: font-lock */
/* End: */
