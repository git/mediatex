#!/bin/bash
# because I'm lazy to upgrade expected results's checks manually

# misc (TODO)

# memory (TODO)
cp tmp/var2/cache/mediatex/mdtx1/git/mdtx1-coll1/catalog000.txt memory/catalogTree.exp
cp memory/catalogTree.out memory/catalogTree.exp2

cp tmp/var/cache/mediatex/mdtx1/git/mdtx1-coll1/extract000.txt memory/extractTree.exp
cp memory/extractTree.out memory/extractTree.exp2

cp memory/recordTree.out memory/recordTree.exp2
cp tmp/var/cache/mediatex/mdtx1/md5sums/mdtx1-coll1.md5 memory/recordTree.exp
cp tmp/var/cache/mediatex/mdtx1/md5sums/mdtx1-coll1.aes memory/recordTree.aes

cp memory/cacheTree.out memory/cacheTree.exp

# parser (TODO)

# common (TODO)
cp common/extractScore.out common/extractScore.exp
cp common/openClose.out common/openClose.exp

# client
for F in $(ls client/*.out); do
    cp $F ${F%.out}.exp
done

# server
for F in $(ls server/*.out); do
    cp $F ${F%.out}.exp
done
