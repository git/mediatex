#!/bin/bash
#=======================================================================
# * Project: MediaTex
# * Module:  client modules (User API)
# *
# * Unit test script for upload.c
#
# MediaTex is an Electronic Records Management System
# Copyright (C) 2014 2015 2016 2017 2018 Nicolas Roche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#=======================================================================
#set -x
set -e

# retrieve environment
[ -z $srcdir ] && srcdir=.
. utmediatex.sh

TEST=$(basename $0)
TEST=${TEST%.sh}

# run the unit test
CONTENT1=$(md5sum $srcdir/../misc/mediatex.css | cut -d' ' -f 1)
CONTENT1=$CONTENT1:$(ls $srcdir/../misc/mediatex.css -l | cut -d' ' -f 5)
CONTENT2=$(md5sum $srcdir/../misc/logo.png | cut -d' ' -f 1)
CONTENT2=$CONTENT2:$(ls $srcdir/../misc/logo.png -l | cut -d' ' -f 5)
CONTENT3=$(md5sum $srcdir/../misc/logo.zip | cut -d' ' -f 1)
CONTENT3=$CONTENT3:$(ls $srcdir/../misc/logo.zip -l | cut -d' ' -f 5)

# new archive
cat >client/$TEST.cat <<EOF
Category "css": "drawing"

Document "css": "css"
  With "designer" = "Me" ""
  $CONTENT1
EOF

# archive already there
cat >client/$TEST.cat2 <<EOF
Document "logo"
  $CONTENT2
EOF

# new archive
cat >client/$TEST.ext <<EOF
(ISO
$CONTENT1
=>
$CONTENT2 there/and/there
)
EOF

# container already there
cat >client/$TEST.ext2 <<EOF
(ZIP
$CONTENT3
=>
$CONTENT2 there/and/there
)
EOF

# archive already there
cat >client/$TEST.ext3 <<EOF
(ZIP
$CONTENT2
=>
$CONTENT2 there/and/there
)
EOF

# internal tests
client/ut$TEST \
    >client/$TEST.out 2>&1

# OK: catalog refers to already known archive
client/ut$TEST \
    -C client/$TEST.cat2 \
    >>client/$TEST.out 2>&1

# KO: catalog cannot refers unknown archive
client/ut$TEST \
    -C client/$TEST.cat \
    >>client/$TEST.out 2>&1 || true

# STUPID (OK): extract catalog refers to already known archive
# (but cannot overload already known containers)
client/ut$TEST \
    -E client/$TEST.ext3 \
    >>client/$TEST.out 2>&1

# KO: extract catalog cannot refers to unknown container
client/ut$TEST \
    -E client/$TEST.ext \
    >>client/$TEST.out 2>&1 || true

# OK: new archive
client/ut$TEST \
    -F $srcdir/../misc/mediatex.css \
    >>client/$TEST.out 2>&1

# OK: upload files alones may overrides existing rules,
# if not already available locally into the cache
client/ut$TEST \
    -F $srcdir/../misc/logo.zip \
    >>client/$TEST.out 2>&1

# KO: same as above, extract catalog cannot refers to unknown container
client/ut$TEST \
    -C client/$TEST.cat \
    -E client/$TEST.ext \
    >>client/$TEST.out 2>&1 || true

# OK: catalogs refers to uploaded file
client/ut$TEST \
    -C client/$TEST.cat \
    -F $srcdir/../misc/mediatex.css \
    >>client/$TEST.out 2>&1

# OK: extract rule refers to uploaded file
client/ut$TEST \
    -E client/$TEST.ext \
    -F $srcdir/../misc/mediatex.css \
    >>client/$TEST.out 2>&1

# KO: uploaded file not described as container by uploaded extract catalog
client/ut$TEST \
    -E client/$TEST.ext3 \
    -F $srcdir/../misc/mediatex.css \
    >>client/$TEST.out 2>&1 || true

# KO: extract cannot overrides existing extract rules
client/ut$TEST \
    -E client/$TEST.ext2 \
    -F $srcdir/../misc/logo.zip \
    >>client/$TEST.out 2>&1 || true

# OK:
client/ut$TEST \
    -C client/$TEST.cat \
    -E client/$TEST.ext \
    -F $srcdir/../misc/mediatex.css \
    >>client/$TEST.out 2>&1

# OK: specify target directory path
client/ut$TEST \
    -F $srcdir/../misc/mediatex.css \
    -T dirname/ \
    >>client/$TEST.out 2>&1

# OK: specify target path
client/ut$TEST \
    -F $srcdir/../misc/mediatex.css \
    -T dirname/filename \
    >>client/$TEST.out 2>&1

# compare with the expected output
mrProperOutputs client/$TEST.out
diff $srcdir/client/$TEST.exp client/$TEST.out
