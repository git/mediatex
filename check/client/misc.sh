#!/bin/bash
#=======================================================================
# * Project: MediaTex
# * Module:  client modules (User API)
# *
# * Unit test script for misc.c
#
# MediaTex is an Electronic Records Management System
# Copyright (C) 2014 2015 2016 2017 2018 Nicolas Roche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#=======================================================================
#set -x
set -e

# retrieve environment
[ -z $srcdir ] && srcdir=.
. utmediatex.sh

TEST=$(basename $0)
TEST=${TEST%.sh}

# add cache.htaccess file
# mkdir -p $GITCLT/mdtx1-coll1/apache2
# cat > $GITCLT/mdtx1-coll1/apache2/cache.htaccess <<EOF
# Content from git
# EOF

# GUI test (if an usused parameter is given)
[ -z "$1" ] || {
    rm -f ${GITCLT}/${MDTXUSER}-coll1/extractNNN.txt
    
    # add images
    if ! grep "a0000000000000000000000000000000:" ${GITCLT}/${MDTXUSER}-coll1/servers.txt;
    then
	for I in $(seq 1 50); do
	    sed ${GITCLT}/${MDTXUSER}-coll1/servers.txt -i -e \
		"/de5008799752552b7963a2670dc5eb18:391168=2.00/ a,a0000000000000000000000000000000:$I=10.00"
	done
    fi

    # add locations
    for I in $(seq 2 49); do
	cat >> ${GITCLT}/${MDTXUSER}-coll1/extractNNN.txt <<EOF
(COPY
 bedac32422739d7eced624ba20f5912e /location$I
=>
 c0000000000000000000000000000000:0 location$IContent
)
EOF
    done
    
    # add archives
    cat >> ${GITCLT}/${MDTXUSER}-coll1/extractNNN.txt <<EOF
(TGZ
 b0000000000000000000000000000000:0
=>
EOF
    for I in $(seq 1 50); do
	cat >> ${GITCLT}/${MDTXUSER}-coll1/extractNNN.txt <<EOF
 b0000000000000000000000000000000:$I archiveContent$I
EOF
    done
    echo ')' >> ${GITCLT}/${MDTXUSER}-coll1/extractNNN.txt

    # add archives to first location
    cat >> ${GITCLT}/${MDTXUSER}-coll1/extractNNN.txt <<EOF
(COPY
 6b18ed0194b0fbadd08e0a13cccda00e /location1
=>
EOF
    for I in $(seq 1 50); do
	cat >> ${GITCLT}/${MDTXUSER}-coll1/extractNNN.txt <<EOF
 d0000000000000000000000000000000:$I location1Content$I
EOF
    done
    echo ')' >> ${GITCLT}/${MDTXUSER}-coll1/extractNNN.txt    
}

# run the unit test
client/ut$TEST >client/$TEST.out 2>&1

# GUI test (if an usused parameter is given)
[ -z "$1" ] || {
    cp ../misc/logo $HOME/mdtx1-coll1/public_html/
    cp ../misc/home.htaccess $HOME/mdtx1-coll1/public_html/.htaccess    
    echo 'SetEnvIf Request_Protocol "^H" NO_AUTH' >> $HOME/mdtx1-coll1/public_html/.htaccess
    cp ../misc/index.htaccess $HOME/mdtx1-coll1/public_html/index/.htaccess
    cp ../misc/cache.htaccess $HOME/mdtx1-coll1/public_html/cache/.htaccess
    cat >> $HOME/mdtx1-coll1/public_html/cache/.htaccess <<EOF
# fancy index for cache
Options +Indexes
SetEnv HOME /test
HeaderName /test/cacheHeader.shtml
ReadmeName /test/footer.html
EOF
    cp ../misc/score.htaccess $HOME/mdtx1-coll1/public_html/score/.htaccess
    cp ../misc/cgi.htaccess $HOME/mdtx1-coll1/public_html/cgi/.htaccess
    cat >$HOME/mdtx1-coll1/public_html/index.shtml <<EOF
<html>
<head>
<meta http-equiv="refresh" content="0; URL=index">
</head>
<body>
</html>
EOF
    mkdir -p $HOME/mdtx1-coll1/public_html/icons
    cp ../misc/floppy-icon.png $HOME/mdtx1-coll1/public_html/icons
    echo "ouf" > $HOME/mdtx1-coll1/public_html/readme.html

    cat <<EOF
please try browsing:
 file://$HOME/mdtx1-coll1/public_html/

or:
 # chgrp www-data -R $HOME/mdtx1-coll1

modify apache configuration:

 Alias /test/ $HOME/mdtx1-coll1/public_html/
 <Directory $HOME/mdtx1-coll1/public_html/>
      AllowOverride All
      Require all granted
 <Directory>

and browse:
 http://localhost/test/index/
EOF

    # remove image
    sed ${GITCLT}/${MDTXUSER}-coll1/servers.txt -i -e \
    	'/a0000000000000000000000000000000:/ d'
    rm -f ${GITCLT}/${MDTXUSER}-coll1/extractNNN.txt
    exit 0
}

# compare with the expected output
mrProperOutputs client/$TEST.out
sed -i client/$TEST.out -e "s/\(audit_20100101-010000_\).*/\1_XXX.txt)/"
diff $srcdir/client/$TEST.exp client/$TEST.out
